package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.endpoint.Role;
import com.tsc.jarinchekhina.tm.endpoint.Session;
import com.tsc.jarinchekhina.tm.endpoint.User;
import com.tsc.jarinchekhina.tm.marker.IntegrationCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;

public class UserEndpointTest {

    @NotNull
    private static final Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private static Session adminSession;

    @BeforeClass
    public static void before() {
        adminSession = bootstrap.getSessionEndpoint().openSession("admin", "admin");
    }

    @AfterClass
    public static void after() {
        bootstrap.getSessionEndpoint().closeSession(adminSession);
    }

    @Test(expected = Exception.class)
    @Category(IntegrationCategory.class)
    public void testCreateUserExists() {
        bootstrap.getUserEndpoint().createUser("test", "autotest");
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testCreateUserWithEmail() {
        @Nullable User user = bootstrap.getAdminUserEndpoint().findByLogin(adminSession, "autotest");
        Assert.assertNull(user);
        user = bootstrap.getUserEndpoint().createUserWithEmail("autotest", "autotest", "autotest@test.ru");
        Assert.assertNotNull(user);

        bootstrap.getAdminUserEndpoint().removeByLogin(adminSession, "autotest");
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testCreateUserWithRole() {
        @Nullable User user = bootstrap.getAdminUserEndpoint().findByLogin(adminSession, "autotest");
        Assert.assertNull(user);
        user = bootstrap.getUserEndpoint().createUserWithRole(adminSession, "autotest", "autotest", Role.USER);
        Assert.assertNotNull(user);

        bootstrap.getAdminUserEndpoint().removeByLogin(adminSession, "autotest");
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testSetPassword() {
        bootstrap.getUserEndpoint().createUser("autotest", "test");
        @Nullable Session session = bootstrap.getSessionEndpoint().openSession("autotest","test");
        Assert.assertNotNull(session);
        bootstrap.getUserEndpoint().setPassword(session, "autotest");
        bootstrap.getSessionEndpoint().closeSession(session);

        session = bootstrap.getSessionEndpoint().openSession("autotest","autotest");
        Assert.assertNotNull(session);
        bootstrap.getSessionEndpoint().closeSession(session);

        bootstrap.getAdminUserEndpoint().removeByLogin(adminSession, "autotest");
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testUpdateUser() {
        bootstrap.getUserEndpoint().createUser("autotest", "autotest");
        @Nullable Session session = bootstrap.getSessionEndpoint().openSession("autotest","autotest");
        Assert.assertNotNull(session);
        @Nullable User user = bootstrap.getUserEndpoint().updateUser(session, "John", "Dow", "Alexander");
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getFirstName());
        Assert.assertEquals("John",user.getFirstName());
        Assert.assertNotNull(user.getLastName());
        Assert.assertEquals("Dow",user.getLastName());
        Assert.assertNotNull(user.getMiddleName());
        Assert.assertEquals("Alexander",user.getMiddleName());
        Assert.assertNull(user.getEmail());
        bootstrap.getSessionEndpoint().closeSession(session);

        bootstrap.getAdminUserEndpoint().removeByLogin(adminSession, "autotest");
    }

}
