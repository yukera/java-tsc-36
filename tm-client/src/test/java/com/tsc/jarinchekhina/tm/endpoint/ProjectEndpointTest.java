package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.endpoint.Project;
import com.tsc.jarinchekhina.tm.endpoint.Session;
import com.tsc.jarinchekhina.tm.endpoint.Status;
import com.tsc.jarinchekhina.tm.marker.IntegrationCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.List;

public class ProjectEndpointTest {

    @NotNull
    private static final Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private static Session session;

    @BeforeClass
    public static void before() {
        bootstrap.getUserEndpoint().createUser("autotest", "autotest");
        session = bootstrap.getSessionEndpoint().openSession("autotest", "autotest");
        Assert.assertNotNull(session);
    }

    @AfterClass
    public static void after() {
        @NotNull final Session adminSession = bootstrap.getSessionEndpoint().openSession("admin", "admin");
        bootstrap.getAdminUserEndpoint().removeByLogin(adminSession, "autotest");
        bootstrap.getSessionEndpoint().closeSession(adminSession);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testCreateFindRemove() {
        @Nullable Project projectById = bootstrap.getProjectEndpoint().createProject(session, "AUTO Project");
        Assert.assertNotNull(projectById);
        @Nullable Project projectByIndex = bootstrap.getProjectEndpoint().createProject(session, "AUTO Project 2");
        Assert.assertNotNull(projectByIndex);
        @Nullable Project projectByName = bootstrap.getProjectEndpoint().createProjectWithDescription(session, "AUTO Project 3", "Desc");
        Assert.assertNotNull(projectByName);
        @NotNull final List<Project> listProjects = bootstrap.getProjectEndpoint().findAllProjects(session);
        Assert.assertEquals(3,listProjects.size());

        @NotNull final String projectId = listProjects.get(0).getId();
        projectById = bootstrap.getProjectEndpoint().findProjectById(session, projectId);
        Assert.assertEquals("AUTO Project", projectById.getName());
        projectByIndex = bootstrap.getProjectEndpoint().findProjectByIndex(session, 1);
        Assert.assertEquals("AUTO Project 2", projectByIndex.getName());
        projectByName = bootstrap.getProjectEndpoint().findProjectByName(session, "AUTO Project 3");
        Assert.assertNotNull(projectByName);

        projectById = bootstrap.getProjectEndpoint().removeProjectById(session, projectId);
        Assert.assertNotNull(projectById);
        projectByIndex = bootstrap.getProjectEndpoint().removeProjectByIndex(session, 0);
        Assert.assertNotNull(projectByIndex);
        projectByName = bootstrap.getProjectEndpoint().removeProjectByName(session, "AUTO Project 3");
        Assert.assertNotNull(projectByName);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testUpdate() {
        @Nullable Project projectById = bootstrap.getProjectEndpoint().createProject(session, "AUTO Project");
        Assert.assertNotNull(projectById);
        @Nullable Project projectByIndex = bootstrap.getProjectEndpoint().createProject(session, "AUTO Project 2");
        Assert.assertNotNull(projectByIndex);

        @NotNull final String projectId = projectById.getId();
        projectById = bootstrap.getProjectEndpoint().updateProjectById(session, projectId, "AUTO","DESC");
        Assert.assertEquals("AUTO", projectById.getName());
        Assert.assertEquals("DESC", projectById.getDescription());

        projectByIndex = bootstrap.getProjectEndpoint().updateProjectByIndex(session, 1, "JUNIT","JDESC");
        Assert.assertEquals("JUNIT", projectByIndex.getName());
        Assert.assertEquals("JDESC", projectByIndex.getDescription());

        projectById = bootstrap.getProjectEndpoint().removeProjectById(session, projectId);
        Assert.assertNotNull(projectById);
        projectByIndex = bootstrap.getProjectEndpoint().removeProjectByIndex(session, 0);
        Assert.assertNotNull(projectByIndex);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testChangeStatus() {
        @Nullable Project projectById = bootstrap.getProjectEndpoint().createProject(session, "AUTO Project");
        Assert.assertNotNull(projectById);
        @Nullable Project projectByIndex = bootstrap.getProjectEndpoint().createProject(session, "AUTO Project 2");
        Assert.assertNotNull(projectByIndex);
        @Nullable Project projectByName = bootstrap.getProjectEndpoint().createProjectWithDescription(session, "AUTO Project 3", "Desc");
        Assert.assertNotNull(projectByName);

        @NotNull final String projectId = projectById.getId();
        Assert.assertEquals(Status.NOT_STARTED, projectById.getStatus());
        projectById = bootstrap.getProjectEndpoint().startProjectById(session, projectId);
        Assert.assertEquals(Status.IN_PROGRESS, projectById.getStatus());
        projectById = bootstrap.getProjectEndpoint().finishProjectById(session, projectId);
        Assert.assertEquals(Status.COMPLETED, projectById.getStatus());
        projectById = bootstrap.getProjectEndpoint().changeProjectStatusById(session, projectId, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectById.getStatus());

        Assert.assertEquals(Status.NOT_STARTED, projectByIndex.getStatus());
        projectByIndex = bootstrap.getProjectEndpoint().startProjectByIndex(session, 1);
        Assert.assertEquals(Status.IN_PROGRESS, projectByIndex.getStatus());
        projectByIndex = bootstrap.getProjectEndpoint().finishProjectByIndex(session, 1);
        Assert.assertEquals(Status.COMPLETED, projectByIndex.getStatus());
        projectByIndex = bootstrap.getProjectEndpoint().changeProjectStatusByIndex(session, 1, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectByIndex.getStatus());

        Assert.assertEquals(Status.NOT_STARTED, projectByName.getStatus());
        projectByName = bootstrap.getProjectEndpoint().startProjectByName(session, "AUTO Project 3");
        Assert.assertEquals(Status.IN_PROGRESS, projectByName.getStatus());
        projectByName = bootstrap.getProjectEndpoint().finishProjectByName(session, "AUTO Project 3");
        Assert.assertEquals(Status.COMPLETED, projectByName.getStatus());
        projectByName = bootstrap.getProjectEndpoint().changeProjectStatusByName(session, "AUTO Project 3", Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectByName.getStatus());

        projectById = bootstrap.getProjectEndpoint().removeProjectById(session, projectId);
        Assert.assertNotNull(projectById);
        projectByIndex = bootstrap.getProjectEndpoint().removeProjectByIndex(session, 0);
        Assert.assertNotNull(projectByIndex);
        projectByName = bootstrap.getProjectEndpoint().removeProjectByName(session, "AUTO Project 3");
        Assert.assertNotNull(projectByName);
    }

}
