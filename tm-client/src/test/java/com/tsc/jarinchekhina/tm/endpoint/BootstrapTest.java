package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class BootstrapTest
{

    @NotNull private final Bootstrap bootstrap = new Bootstrap();

    @Test
    @Category(UnitCategory.class)
    public void testGetters() {
        Assert.assertNotNull(bootstrap.getAdminEndpoint());
        Assert.assertNotNull(bootstrap.getAdminUserEndpoint());
        Assert.assertNotNull(bootstrap.getSessionEndpoint());
        Assert.assertNotNull(bootstrap.getProjectEndpoint());
        Assert.assertNotNull(bootstrap.getTaskEndpoint());
        Assert.assertNotNull(bootstrap.getUserEndpoint());
        Assert.assertNull(bootstrap.getSession());
    }

    @Category(UnitCategory.class)
    @Test(expected = AccessDeniedException.class)
    public void testUserIdException() {
        bootstrap.getUserId();
    }

}
