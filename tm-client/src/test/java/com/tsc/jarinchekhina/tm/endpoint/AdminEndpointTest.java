package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.endpoint.Session;
import com.tsc.jarinchekhina.tm.marker.IntegrationCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;

public class AdminEndpointTest {

    @NotNull
    private static final Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private static Session session;

    @Before
    public void before() {
        session = bootstrap.getSessionEndpoint().openSession("admin", "admin");
        Assert.assertNotNull(session);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testBackup() {
        bootstrap.getAdminEndpoint().saveBackup(session);
        bootstrap.getAdminEndpoint().loadBackup(session);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testBinaryData() {
        bootstrap.getAdminEndpoint().saveBinaryData(session);
        bootstrap.getAdminEndpoint().loadBinaryData(session);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testBase64Data() {
        bootstrap.getAdminEndpoint().saveBase64Data(session);
        bootstrap.getAdminEndpoint().loadBase64Data(session);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testFasterXmlData() {
        bootstrap.getAdminEndpoint().saveFasterXmlData(session, false);
        bootstrap.getAdminEndpoint().loadFasterXmlData(session, false);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testFasterXmlJsonData() {
        bootstrap.getAdminEndpoint().saveFasterXmlData(session, true);
        bootstrap.getAdminEndpoint().loadFasterXmlData(session, true);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testJaxbData() {
        bootstrap.getAdminEndpoint().saveJaxbData(session, false);
        bootstrap.getAdminEndpoint().loadJaxbData(session, false);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testJaxbJsonData() {
        bootstrap.getAdminEndpoint().saveJaxbData(session, true);
        bootstrap.getAdminEndpoint().loadJaxbData(session, true);
    }

}
