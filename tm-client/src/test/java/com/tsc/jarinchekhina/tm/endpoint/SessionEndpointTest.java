package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.endpoint.Session;
import com.tsc.jarinchekhina.tm.endpoint.User;
import com.tsc.jarinchekhina.tm.marker.IntegrationCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class SessionEndpointTest {

    @NotNull
    private final Bootstrap bootstrap = new Bootstrap();

    @Test
    @Category(IntegrationCategory.class)
    public void testSession() {
        @NotNull final String login = "test";
        @Nullable Session session = bootstrap.getSessionEndpoint().openSession(login, "test");
        Assert.assertNotNull(session);

        @Nullable User user = bootstrap.getSessionEndpoint().getUser(session);
        Assert.assertNotNull(user);
        Assert.assertEquals(login,user.getLogin());

        bootstrap.getSessionEndpoint().closeSession(session);
    }

    @Test(expected = Exception.class)
    @Category(IntegrationCategory.class)
    public void testIncorrectLogin() {
        bootstrap.getSessionEndpoint().openSession("testinc", "test");
    }

    @Test(expected = Exception.class)
    @Category(IntegrationCategory.class)
    public void testIncorrectPassword() {
        bootstrap.getSessionEndpoint().openSession("test", "testinc");
    }

}
