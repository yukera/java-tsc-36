package com.tsc.jarinchekhina.tm.command.data;

import com.tsc.jarinchekhina.tm.command.AbstractDataCommand;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataJsonLoadJaxbCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-json-jaxb-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load JSON data from file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[LOAD JSON DATA]");
        serviceLocator.getAdminEndpoint().loadJaxbData(serviceLocator.getSession(), true);
        serviceLocator.setSession(null);
    }

}
