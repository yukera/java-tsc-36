package com.tsc.jarinchekhina.tm.api.repository;

import com.tsc.jarinchekhina.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.List;

public interface ICommandRepository {

    @NotNull
    List<AbstractCommand> getCommandList();

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<AbstractCommand> getArguments();

    @NotNull
    Collection<String> getCommandNames();

    @NotNull
    Collection<String> getCommandArgs();

    @Nullable
    AbstractCommand getCommandByName(@NotNull String name);

    @Nullable
    AbstractCommand getCommandByArg(@NotNull String name);

    void add(@NotNull AbstractCommand command);

}