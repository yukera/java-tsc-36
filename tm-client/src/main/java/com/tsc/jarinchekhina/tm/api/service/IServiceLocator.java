package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    UserEndpoint getUserEndpoint();

    @NotNull
    AdminEndpoint getAdminEndpoint();

    @NotNull
    AdminUserEndpoint getAdminUserEndpoint();

    @NotNull
    Session getSession();

    void setSession(@Nullable Session session);

}
