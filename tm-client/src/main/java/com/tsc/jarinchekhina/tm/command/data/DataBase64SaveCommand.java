package com.tsc.jarinchekhina.tm.command.data;

import com.tsc.jarinchekhina.tm.command.AbstractDataCommand;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataBase64SaveCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-base64-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save BASE64 data to file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[SAVE BASE64 DATA]");
        serviceLocator.getAdminEndpoint().saveBase64Data(serviceLocator.getSession());
    }

}
