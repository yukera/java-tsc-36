package com.tsc.jarinchekhina.tm.command.data;

import com.tsc.jarinchekhina.tm.command.AbstractDataCommand;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataBinarySaveCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-binary-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save binary data to file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[SAVE BINARY DATA]");
        serviceLocator.getAdminEndpoint().saveBinaryData(serviceLocator.getSession());
    }

}
