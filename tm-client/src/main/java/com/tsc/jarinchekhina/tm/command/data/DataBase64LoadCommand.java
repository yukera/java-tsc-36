package com.tsc.jarinchekhina.tm.command.data;

import com.tsc.jarinchekhina.tm.command.AbstractDataCommand;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataBase64LoadCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-base64-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load BASE64 data from file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[LOAD BASE64 DATA]");
        serviceLocator.getAdminEndpoint().loadBase64Data(serviceLocator.getSession());
        serviceLocator.setSession(null);
    }

}
