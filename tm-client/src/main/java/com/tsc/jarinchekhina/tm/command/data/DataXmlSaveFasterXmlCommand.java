package com.tsc.jarinchekhina.tm.command.data;

import com.tsc.jarinchekhina.tm.command.AbstractDataCommand;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataXmlSaveFasterXmlCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-xml-faster-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save XML data to file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[SAVE XML DATA]");
        serviceLocator.getAdminEndpoint().saveFasterXmlData(serviceLocator.getSession(), false);
    }

}
