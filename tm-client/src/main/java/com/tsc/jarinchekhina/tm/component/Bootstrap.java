package com.tsc.jarinchekhina.tm.component;

import com.tsc.jarinchekhina.tm.api.repository.ICommandRepository;
import com.tsc.jarinchekhina.tm.api.service.ICommandService;
import com.tsc.jarinchekhina.tm.api.service.IServiceLocator;
import com.tsc.jarinchekhina.tm.command.AbstractCommand;
import com.tsc.jarinchekhina.tm.endpoint.*;
import com.tsc.jarinchekhina.tm.exception.system.UnknownCommandException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.repository.CommandRepository;
import com.tsc.jarinchekhina.tm.service.CommandService;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@Getter
@Setter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();

    @NotNull
    private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();

    @NotNull
    private final AdminEndpointService adminEndpointService = new AdminEndpointService();

    @NotNull
    private final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();

    @NotNull
    private final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();

    @Nullable
    private Session session = null;

    @Nullable
    @SneakyThrows
    public String getUserId() {
        if (session == null) throw new AccessDeniedException();
        return session.getUserId();
    }

    private void initCommands(@NotNull final List<AbstractCommand> commandList) {
        for (@NotNull AbstractCommand command : commandList) initCommand(command);
    }

    private void initCommand(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @SneakyThrows
    public void parseCommand(@NotNull final String cmd) {
        if (DataUtil.isEmpty(cmd)) throw new UnknownCommandException(cmd);
        @Nullable final AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        command.execute();
    }

    @SneakyThrows
    private void start() {
        System.out.println("*** Welcome to Task Manager ***");
        while (true) {
            try {
                @NotNull final String command = TerminalUtil.nextLine();
                parseCommand(command);
                System.out.println("[OK]");
            } catch (Exception e) {
                System.err.println("[FAIL]");
            }
        }
    }

    @SneakyThrows
    public void init() {
        initCommands(commandService.getCommandList());
        start();
    }

}