package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.entity.Project;
import com.tsc.jarinchekhina.tm.entity.Task;
import com.tsc.jarinchekhina.tm.entity.User;
import com.tsc.jarinchekhina.tm.enumerated.Sort;
import org.jetbrains.annotations.NotNull;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;

import java.util.List;

@FixMethodOrder
public class TaskRepositoryTest {

    @NotNull
    private static final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private static final User user = bootstrap.getUserService().create("auto", "auto");

    @NotNull
    private static final String userId = user.getId();

    @AfterClass
    public static void after() {
        bootstrap.getUserRepository().removeByLogin("auto");
    }

    @Test
    public void testAddFindAll() {
        @NotNull final Project project = bootstrap.getProjectService().create(userId, "AUTO Project");
        @NotNull final Task taskSecond = new Task();
        taskSecond.setName("AUTO 2");
        taskSecond.setProjectId(project.getId());
        bootstrap.getTaskRepository().add(userId, taskSecond);
        @NotNull final Task taskFirst = new Task();
        taskFirst.setName("AUTO 1");
        bootstrap.getTaskRepository().add(userId, taskFirst);

        @NotNull List<Task> taskList = bootstrap.getTaskRepository().findAll(userId);
        Assert.assertEquals(2, taskList.size());
        taskList = bootstrap.getTaskRepository().findAll(userId, Sort.NAME.getComparator());
        Assert.assertEquals("AUTO 1", taskList.get(0).getName());

        taskList = bootstrap.getTaskRepository().findAllByProjectId(userId, project.getId());
        Assert.assertEquals(1, taskList.size());
        Assert.assertEquals("AUTO 2", taskList.get(0).getName());

        bootstrap.getTaskRepository().removeAllByProjectId(project.getId());
        taskList = bootstrap.getTaskRepository().findAll(userId);
        Assert.assertEquals(1, taskList.size());
        Assert.assertEquals("AUTO 1", taskList.get(0).getName());
        bootstrap.getTaskRepository().removeByName(userId, "AUTO 1");
    }

    @Test
    public void testFindRemove() {
        @NotNull Task taskById = new Task();
        taskById.setName("AUTO Id");
        bootstrap.getTaskRepository().add(userId, taskById);
        @NotNull Task taskByIndex = new Task();
        taskByIndex.setName("AUTO Index");
        bootstrap.getTaskRepository().add(userId, taskByIndex);
        @NotNull Task taskByName = new Task();
        taskByName.setName("AUTO Name");
        bootstrap.getTaskRepository().add(userId, taskByName);

        taskById = bootstrap.getTaskRepository().findById(userId, taskById.getId()).get();
        Assert.assertEquals("AUTO Id", taskById.getName());
        taskByIndex = bootstrap.getTaskRepository().findByIndex(userId, 1).get();
        Assert.assertEquals("AUTO Index", taskByIndex.getName());
        taskByName = bootstrap.getTaskRepository().findByName(userId, "AUTO Name").get();
        Assert.assertEquals("AUTO Name", taskByName.getName());

        bootstrap.getTaskRepository().removeById(userId, taskById.getId());
        bootstrap.getTaskRepository().removeByIndex(userId, 0);
        bootstrap.getTaskRepository().removeByName(userId, "AUTO Name");

        @NotNull final List<Task> taskList = bootstrap.getTaskRepository().findAll(userId);
        Assert.assertEquals(0, taskList.size());
    }

}
