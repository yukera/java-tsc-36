package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.service.IDataService;
import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.entity.Task;
import com.tsc.jarinchekhina.tm.entity.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Optional;

public class DataServiceTest {

    @NotNull
    private static final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private static final String TASK_BACKUP = "AUTO Task Backup Check";

    @NotNull
    private static final String TASK_BINARY = "AUTO Task Binary Check";

    @NotNull
    private static final String TASK_BASE64 = "AUTO Task Base64 Check";

    @NotNull
    private static final String TASK_XML_FASTER = "AUTO Task Faster Xml Xml Check";

    @NotNull
    private static final String TASK_XML_JAXB = "AUTO Task Jaxb Xml Check";

    @NotNull
    private static final String TASK_JSON_FASTER = "AUTO Task Faster Xml Json Check";

    @NotNull
    private static final String TASK_JSON_JAXB = "AUTO Task Jaxb Json Check";

    @Nullable
    private static String userId;

    @BeforeClass
    public static void beforeClass() {
        bootstrap.getDataService().loadBackup();
        @NotNull final User user = bootstrap.getUserService().create("auto", "auto");
        userId = user.getId();
    }

    @AfterClass
    public static void afterClass() {
        bootstrap.getTaskService().clear(userId);
        bootstrap.getUserRepository().removeByLogin("auto");
        bootstrap.getDataService().saveBackup();
    }

    @Test
    public void testBackup() {
        bootstrap.getTaskService().create(userId, TASK_BACKUP);
        bootstrap.getDataService().saveBackup();
        bootstrap.getTaskService().clear(userId);
        bootstrap.getDataService().loadBackup();
        @NotNull final Optional<Task> task = bootstrap.getTaskService().findByName(userId, TASK_BACKUP);
        Assert.assertTrue(task.isPresent());
    }

    @Test
    public void testBinary() {
        bootstrap.getTaskService().create(userId, TASK_BINARY);
        bootstrap.getDataService().saveBinaryData();
        bootstrap.getTaskService().clear(userId);
        bootstrap.getDataService().loadBinaryData();
        @NotNull final Optional<Task> task = bootstrap.getTaskService().findByName(userId, TASK_BINARY);
        Assert.assertTrue(task.isPresent());
    }

    @Test
    public void testBase64() {
        bootstrap.getTaskService().create(userId, TASK_BASE64);
        bootstrap.getDataService().saveBase64Data();
        bootstrap.getTaskService().clear(userId);
        bootstrap.getDataService().loadBase64Data();
        @NotNull final Optional<Task> task = bootstrap.getTaskService().findByName(userId, TASK_BASE64);
        Assert.assertTrue(task.isPresent());
    }

    @Test
    public void testFasterXml() {
        bootstrap.getTaskService().create(userId, TASK_XML_FASTER);
        bootstrap.getDataService().saveFasterXmlData(false);
        bootstrap.getTaskService().clear(userId);
        bootstrap.getDataService().loadFasterXmlData(false);
        @NotNull Optional<Task> task = bootstrap.getTaskService().findByName(userId, TASK_XML_FASTER);
        Assert.assertTrue(task.isPresent());

        bootstrap.getTaskService().create(userId, TASK_JSON_FASTER);
        bootstrap.getDataService().saveFasterXmlData(true);
        bootstrap.getTaskService().clear(userId);
        bootstrap.getDataService().loadFasterXmlData(true);
        task = bootstrap.getTaskService().findByName(userId, TASK_JSON_FASTER);
        Assert.assertTrue(task.isPresent());
    }

    @Test
    public void testJaxb() {
        bootstrap.getTaskService().create(userId, TASK_XML_JAXB);
        bootstrap.getDataService().saveJaxbData(false);
        bootstrap.getTaskService().clear(userId);
        bootstrap.getDataService().loadJaxbData(false);
        @NotNull Optional<Task> task = bootstrap.getTaskService().findByName(userId, TASK_XML_JAXB);
        Assert.assertTrue(task.isPresent());

        bootstrap.getTaskService().create(userId, TASK_JSON_JAXB);
        bootstrap.getDataService().saveJaxbData(true);
        bootstrap.getTaskService().clear(userId);
        bootstrap.getDataService().loadJaxbData(true);
        task = bootstrap.getTaskService().findByName(userId, TASK_JSON_JAXB);
        Assert.assertTrue(task.isPresent());
    }

}
