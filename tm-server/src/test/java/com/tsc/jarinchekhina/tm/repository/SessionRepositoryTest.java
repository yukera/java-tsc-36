package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.entity.Session;
import com.tsc.jarinchekhina.tm.entity.User;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class SessionRepositoryTest {

    @NotNull
    private static final Bootstrap bootstrap = new Bootstrap();

    @Test
    public void testFindSession() {
        @NotNull final User user = bootstrap.getUserService().create("auto", "auto");
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        bootstrap.getSessionRepository().add(session);

        @NotNull List<Session> sessionList = bootstrap.getSessionRepository().findByUserId(user.getId());
        Assert.assertEquals(1, sessionList.size());
        sessionList = bootstrap.getSessionRepository().findByUserId("1");
        Assert.assertEquals(0, sessionList.size());

        bootstrap.getSessionRepository().clear();
        sessionList = bootstrap.getSessionRepository().findAll();
        Assert.assertEquals(0, sessionList.size());
    }

}
