package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.service.IDataService;
import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.entity.Session;
import com.tsc.jarinchekhina.tm.entity.User;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyLoginException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyPasswordException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class SessionServiceTest {

    @NotNull
    private static final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private static final User user = bootstrap.getUserService().create("auto", "auto");

    @AfterClass
    public static void afterClass() {
        bootstrap.getUserRepository().removeByLogin("auto");
        bootstrap.getUserRepository().removeByLogin("auto locked");
    }

    @After
    public void after() {
        bootstrap.getSessionRepository().clear();
    }

    @Test
    public void testCheckDataAccess() {
        boolean result = bootstrap.getSessionService().checkDataAccess("", "auto");
        Assert.assertFalse(result);
        result = bootstrap.getSessionService().checkDataAccess("auto", "");
        Assert.assertFalse(result);
        result = bootstrap.getSessionService().checkDataAccess("autooo","auto");
        Assert.assertFalse(result);
        result = bootstrap.getSessionService().checkDataAccess("auto","autooo");
        Assert.assertFalse(result);
        result = bootstrap.getSessionService().checkDataAccess("auto","auto");
        Assert.assertTrue(result);
    }

    @Test(expected = EmptyLoginException.class)
    public void testOpenSessionNoLogin() {
        bootstrap.getSessionService().open("","auto");
    }

    @Test(expected = EmptyPasswordException.class)
    public void testOpenSessionNoPassword() {
        bootstrap.getSessionService().open("auto","");
    }

    @Test(expected = AccessDeniedException.class)
    public void testOpenSessionNoUser() {
        bootstrap.getSessionService().open("autoooo","auto");
    }

    @Test(expected = AccessDeniedException.class)
    public void testOpenSessionWrongPassword() {
        bootstrap.getSessionService().open("auto","autoooo");
    }

    @Test(expected = AccessDeniedException.class)
    public void testOpenSessionUserLocked() {
        bootstrap.getUserService().create("auto locked", "auto");
        bootstrap.getUserService().lockByLogin("auto locked");
        bootstrap.getSessionService().open("auto locked","auto");
    }

    @Test
    public void testOpenSignSession() {
        @Nullable Session session = bootstrap.getSessionService().open("auto", "auto");
        Assert.assertNotNull(session);
        @Nullable final String signature = session.getSignature();
        Assert.assertNotNull(signature);
        session.setSignature(null);
        session = bootstrap.getSessionService().sign(session);
        Assert.assertNotNull(session);
        Assert.assertEquals(signature, session.getSignature());
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateSessionNull() {
        bootstrap.getSessionService().validate(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateSessionNoSignature() {
        @Nullable final Session session = bootstrap.getSessionService().open("auto", "auto");
        Assert.assertNotNull(session);
        session.setSignature(null);
        bootstrap.getSessionService().validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateSessionNoUserId() {
        @Nullable final Session session = bootstrap.getSessionService().open("auto", "auto");
        Assert.assertNotNull(session);
        session.setUserId(null);
        bootstrap.getSessionService().validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateSessionNoTimeStamp() {
        @Nullable final Session session = bootstrap.getSessionService().open("auto", "auto");
        Assert.assertNotNull(session);
        session.setTimestamp(null);
        bootstrap.getSessionService().validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateSessionWrongSignature() {
        @Nullable final Session session = bootstrap.getSessionService().open("auto", "auto");
        Assert.assertNotNull(session);
        session.setSignature("1111");
        bootstrap.getSessionService().validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateSessionNoSessionInRepo() {
        @Nullable final Session session = bootstrap.getSessionService().open("auto", "auto");
        Assert.assertNotNull(session);
        bootstrap.getSessionService().close(session);
        bootstrap.getSessionService().validate(session);
    }

    @Test
    public void testValidateSession() {
        @Nullable final Session session = bootstrap.getSessionService().open("auto", "auto");
        Assert.assertNotNull(session);
        bootstrap.getSessionService().validate(session);
    }

    @Test
    public void testIsValid() {
        @Nullable Session session = bootstrap.getSessionService().open("auto", "auto");
        Assert.assertNotNull(session);
        boolean result = bootstrap.getSessionService().isValid(session);
        Assert.assertTrue(result);
        session.setSignature(null);
        result = bootstrap.getSessionService().isValid(session);
        Assert.assertFalse(result);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateSessionWithRoleNull() {
        @Nullable final Session session = bootstrap.getSessionService().open("auto", "auto");
        Assert.assertNotNull(session);
        bootstrap.getSessionService().validate(session, null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateSessionWithRoleNoUserId() {
        @Nullable final Session session = bootstrap.getSessionService().open("auto", "auto");
        Assert.assertNotNull(session);
        session.setUserId(null);
        bootstrap.getSessionService().validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateSessionWithRoleNoUserInRepo() {
        bootstrap.getUserService().create("auto 2", "auto");
        @Nullable final Session session = bootstrap.getSessionService().open("auto 2", "auto");
        Assert.assertNotNull(session);
        bootstrap.getUserRepository().removeByLogin("auto 2");
        bootstrap.getSessionService().validate(session, Role.USER);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateSessionWithRoleIncorrect() {
        @Nullable final Session session = bootstrap.getSessionService().open("auto", "auto");
        Assert.assertNotNull(session);
        bootstrap.getSessionService().validate(session, Role.ADMIN);
    }

    @Test
    public void testValidateSessionWithRole() {
        @Nullable final Session session = bootstrap.getSessionService().open("auto", "auto");
        Assert.assertNotNull(session);
        bootstrap.getSessionService().validate(session, Role.USER);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetUserNoUser() {
        bootstrap.getUserService().create("auto 2", "auto");
        @Nullable final Session session = bootstrap.getSessionService().open("auto 2", "auto");
        Assert.assertNotNull(session);
        bootstrap.getUserRepository().removeByLogin("auto 2");
        bootstrap.getSessionService().getUser(session);
    }

    @Test
    public void testGetUser() {
        @Nullable final Session session = bootstrap.getSessionService().open("auto", "auto");
        Assert.assertNotNull(session);
        @NotNull final User userFromSession = bootstrap.getSessionService().getUser(session);
        Assert.assertNotNull(userFromSession);
        Assert.assertEquals(user.getId(), userFromSession.getId());
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetUserIdNoId() {
        @Nullable final Session session = bootstrap.getSessionService().open("auto", "auto");
        Assert.assertNotNull(session);
        session.setUserId(null);
        bootstrap.getSessionService().getUserId(session);
    }

    @Test
    public void testGetUserId() {
        @Nullable final Session session = bootstrap.getSessionService().open("auto", "auto");
        Assert.assertNotNull(session);
        @NotNull final String userId = bootstrap.getSessionService().getUserId(session);
        Assert.assertEquals(user.getId(), userId);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetListSessionValidationFail() {
        @Nullable final Session session = bootstrap.getSessionService().open("auto", "auto");
        Assert.assertNotNull(session);
        session.setSignature("1111");
        bootstrap.getSessionService().getListSession(session);
    }

    @Test
    public void testGetListSession() {
        @Nullable final Session session = bootstrap.getSessionService().open("auto", "auto");
        @Nullable final Session sessionSecond = bootstrap.getSessionService().open("auto", "auto");
        Assert.assertNotNull(session);
        Assert.assertNotNull(sessionSecond);
        @NotNull final List<Session> sessionList = bootstrap.getSessionService().getListSession(session);
        Assert.assertEquals(2, sessionList.size());
        Assert.assertEquals(session.getId(), sessionList.get(0).getId());
        Assert.assertEquals(sessionSecond.getId(), sessionList.get(1).getId());
    }

    @Test(expected = AccessDeniedException.class)
    public void testCloseSessionValidationFail() {
        @Nullable final Session session = bootstrap.getSessionService().open("auto", "auto");
        Assert.assertNotNull(session);
        session.setSignature("1111");
        bootstrap.getSessionService().close(session);
    }

    @Test
    public void testCloseSession() {
        @Nullable final Session session = bootstrap.getSessionService().open("auto", "auto");
        Assert.assertNotNull(session);
        boolean result = bootstrap.getSessionService().isValid(session);
        Assert.assertTrue(result);
        bootstrap.getSessionService().close(session);
        result = bootstrap.getSessionService().isValid(session);
        Assert.assertFalse(result);
    }

    @Test(expected = AccessDeniedException.class)
    public void testCloseSessionClosed() {
        @Nullable final Session session = bootstrap.getSessionService().open("auto", "auto");
        Assert.assertNotNull(session);
        bootstrap.getSessionService().close(session);
        bootstrap.getSessionService().close(session);
    }

    @Test
    public void testCloseAllSessions() {
        @Nullable final Session session = bootstrap.getSessionService().open("auto", "auto");
        @Nullable final Session sessionSecond = bootstrap.getSessionService().open("auto", "auto");
        Assert.assertNotNull(session);
        Assert.assertNotNull(sessionSecond);
        @NotNull final List<Session> sessionList = bootstrap.getSessionService().getListSession(session);
        Assert.assertEquals(2, sessionList.size());
        @Nullable final Session sessionThird = bootstrap.getSessionService().open("auto", "auto");
        Assert.assertNotNull(sessionThird);

        bootstrap.getSessionService().closeAll(sessionList);
        boolean result = bootstrap.getSessionService().isValid(session);
        Assert.assertFalse(result);
        result = bootstrap.getSessionService().isValid(sessionThird);
        Assert.assertTrue(result);
    }

    @Test
    public void testCloseAllSessionsSomeClosed() {
        @Nullable final Session session = bootstrap.getSessionService().open("auto", "auto");
        @Nullable final Session sessionSecond = bootstrap.getSessionService().open("auto", "auto");
        @Nullable final Session sessionThird = bootstrap.getSessionService().open("auto", "auto");
        Assert.assertNotNull(session);
        Assert.assertNotNull(sessionSecond);
        Assert.assertNotNull(sessionThird);
        @NotNull final List<Session> sessionList = bootstrap.getSessionService().getListSession(session);
        Assert.assertEquals(3, sessionList.size());

        bootstrap.getSessionService().close(sessionSecond);
        bootstrap.getSessionService().closeAll(sessionList);
    }

}
