package com.tsc.jarinchekhina.tm.service;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.entity.User;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.exception.empty.*;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.exception.user.EmailRegisteredException;
import com.tsc.jarinchekhina.tm.exception.user.LoginExistsException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Optional;

public class UserServiceTest {

    @NotNull
    private static final Bootstrap bootstrap = new Bootstrap();

    @BeforeClass
    public static void before() {
        bootstrap.getDataService().loadBackup();
    }

    @Test(expected = EmptyLoginException.class)
    public void testCreateNoLogin() {
        bootstrap.getUserService().create("","auto");
    }

    @Test(expected = EmptyPasswordException.class)
    public void testCreateNoPassword() {
        bootstrap.getUserService().create("auto","");
    }

    @Test(expected = LoginExistsException.class)
    public void testCreateLoginExists() {
        bootstrap.getUserService().create("test","test");
    }

    @Test
    public void testCreateFindRemove() {
        @NotNull User user = bootstrap.getUserService().create("auto","auto");
        @NotNull Optional<User> userByLogin = bootstrap.getUserService().findByLogin("auto");
        Assert.assertTrue(userByLogin.isPresent());
        Assert.assertEquals(user.getId(), userByLogin.get().getId());

        userByLogin = bootstrap.getUserService().removeByLogin("auto");
        Assert.assertTrue(userByLogin.isPresent());
        Assert.assertEquals(user.getId(), userByLogin.get().getId());
        userByLogin = bootstrap.getUserService().findByLogin("auto");
        Assert.assertFalse(userByLogin.isPresent());
    }

    @Test(expected = EmptyLoginException.class)
    public void testCreateWithEmailNoLogin() {
        bootstrap.getUserService().create("","auto", "autott");
    }

    @Test(expected = EmptyPasswordException.class)
    public void testCreateWithEmailNoPassword() {
        bootstrap.getUserService().create("auto","", "autott");
    }

    @Test(expected = EmptyEmailException.class)
    public void testCreateWithEmailNoEmail() {
        bootstrap.getUserService().create("auto","auto", "");
    }

    @Test(expected = LoginExistsException.class)
    public void testCreateWithEmailLoginExists() {
        bootstrap.getUserService().create("test","test", "autott");
    }

    @Test(expected = EmailRegisteredException.class)
    public void testCreateWithEmailRegisteredEmail() {
        bootstrap.getUserService().create("auto","auto", "test@test.ru");
    }

    @Test
    public void testCreateFindWithEmailFind() {
        @NotNull final User user = bootstrap.getUserService().create("auto","auto", "autott");
        @NotNull Optional<User> userByLogin = bootstrap.getUserService().findByEmail("autott");
        Assert.assertTrue(userByLogin.isPresent());
        Assert.assertEquals(user.getId(), userByLogin.get().getId());

        userByLogin = bootstrap.getUserService().removeByLogin("auto");
        Assert.assertTrue(userByLogin.isPresent());
    }

    @Test(expected = EmptyLoginException.class)
    public void testCreateWithRoleNoLogin() {
        bootstrap.getUserService().create("","auto", Role.USER);
    }

    @Test(expected = EmptyPasswordException.class)
    public void testCreateWithRoleNoPassword() {
        bootstrap.getUserService().create("auto","", Role.USER);
    }

    @Test(expected = LoginExistsException.class)
    public void testCreateWithRoleLoginExists() {
        bootstrap.getUserService().create("test","test", Role.USER);
    }

    @Test(expected = EmptyRoleException.class)
    public void testCreateWithRoleNull() {
        @Nullable final Role role = null;
        bootstrap.getUserService().create("auto","auto", role);
    }

    @Test
    public void testCreateCheckRole() {
        @NotNull final User user = bootstrap.getUserService().create("auto","auto", Role.USER);
        boolean result = bootstrap.getUserService().checkRoles(user.getId(), Role.ADMIN);
        Assert.assertFalse(result);
        result = bootstrap.getUserService().checkRoles(user.getId(), Role.USER);
        Assert.assertTrue(result);
        bootstrap.getUserService().removeByLogin("auto");
    }

    @Test(expected = AccessDeniedException.class)
    public void testCheckRoleNoUser() {
        bootstrap.getUserService().checkRoles("111", Role.USER);
    }

    @Test(expected = EmptyLoginException.class)
    public void testLockByLoginNoLogin() {
        bootstrap.getUserService().lockByLogin("");
    }

    @Test(expected = AccessDeniedException.class)
    public void testLockByLoginNoUser() {
        bootstrap.getUserService().lockByLogin("auto");
    }

    @Test(expected = EmptyLoginException.class)
    public void testUnlockByLoginNoLogin() {
        bootstrap.getUserService().unlockByLogin("");
    }

    @Test(expected = AccessDeniedException.class)
    public void testUnlockByLoginNoUser() {
        bootstrap.getUserService().unlockByLogin("auto");
    }

    @Test
    public void testLockUnlockUser() {
        @NotNull final User user = bootstrap.getUserService().create("auto","auto");
        Assert.assertNotNull(user);
        Assert.assertFalse(user.isLocked());

        @NotNull Optional<User> userOptional = bootstrap.getUserService().lockByLogin("auto");
        Assert.assertTrue(userOptional.isPresent());
        Assert.assertTrue(userOptional.get().isLocked());
        userOptional = bootstrap.getUserService().unlockByLogin("auto");
        Assert.assertTrue(userOptional.isPresent());
        Assert.assertFalse(userOptional.get().isLocked());

        bootstrap.getUserService().removeByLogin("auto");
    }

    @Test(expected = EmptyIdException.class)
    public void testSetPasswordNoId() {
        bootstrap.getUserService().setPassword("","123");
    }

    @Test(expected = EmptyPasswordException.class)
    public void testSetPasswordNoPassword() {
        bootstrap.getUserService().setPassword("111","");
    }

    @Test(expected = AccessDeniedException.class)
    public void testSetPasswordNoUser() {
        bootstrap.getUserService().setPassword("111","123");
    }

    @Test
    public void testSetPassword() {
        @NotNull User user = bootstrap.getUserService().create("auto","auto");
        Assert.assertNotNull(user);
        @NotNull final String oldPasswordHash = user.getPasswordHash();
        user = bootstrap.getUserService().setPassword(user.getId(), "123");
        Assert.assertNotEquals(oldPasswordHash, user.getPasswordHash());
        bootstrap.getUserService().removeByLogin("auto");
    }

    @Test(expected = EmptyIdException.class)
    public void testUpdateUserNoId() {
        bootstrap.getUserService().updateUser("","a","b","c");
    }

    @Test(expected = AccessDeniedException.class)
    public void testUpdateUserNoUser() {
        bootstrap.getUserService().updateUser("111","a","b","c");
    }

    @Test
    public void testUpdateUser() {
        @NotNull User user = bootstrap.getUserService().create("auto","auto");
        Assert.assertNotNull(user);
        Assert.assertNull(user.getFirstName());
        Assert.assertNull(user.getLastName());
        Assert.assertNull(user.getMiddleName());

        user = bootstrap.getUserService().updateUser(user.getId(),"aaa","bbb","ccc");
        Assert.assertNotNull(user);
        Assert.assertEquals("aaa", user.getFirstName());
        Assert.assertEquals("bbb", user.getLastName());
        Assert.assertEquals("ccc", user.getMiddleName());
        bootstrap.getUserService().removeByLogin("auto");
    }

}
