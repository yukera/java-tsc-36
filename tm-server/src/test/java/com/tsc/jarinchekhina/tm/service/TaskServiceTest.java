package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.entity.Project;
import com.tsc.jarinchekhina.tm.entity.Task;
import com.tsc.jarinchekhina.tm.entity.User;
import com.tsc.jarinchekhina.tm.enumerated.Sort;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyDescriptionException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyNameException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyStatusException;
import com.tsc.jarinchekhina.tm.exception.entity.TaskNotFoundException;
import com.tsc.jarinchekhina.tm.exception.system.IndexIncorrectException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

public class TaskServiceTest {

    @NotNull
    private static final Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private static String userId;

    @BeforeClass
    public static void beforeClass() {
        @NotNull final User user = bootstrap.getUserService().create("auto", "auto");
        userId = user.getId();
    }

    @AfterClass
    public static void afterClass() {
        bootstrap.getTaskService().clear(userId);
        bootstrap.getUserRepository().removeByLogin("auto");
    }

    @Test(expected = AccessDeniedException.class)
    public void testFindAllNoUserId() {
        bootstrap.getTaskService().findAll("");
    }

    @Test(expected = AccessDeniedException.class)
    public void testFindAllWithComparatorNoUserId() {
        bootstrap.getTaskService().findAll("", Sort.NAME.getComparator());
    }

    @Test(expected = AccessDeniedException.class)
    public void testAddNoUserId() {
        bootstrap.getTaskService().add("", null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testCreateNoUserId() {
        bootstrap.getTaskService().create("", "AUTO Task");
    }

    @Test(expected = EmptyNameException.class)
    public void testCreateNoName() {
        bootstrap.getTaskService().create(userId, "");
    }

    @Test(expected = AccessDeniedException.class)
    public void testCreateWithDescriptionNoUserId() {
        bootstrap.getTaskService().create("", "AUTO Task", "Desc");
    }

    @Test(expected = EmptyNameException.class)
    public void testCreateWithDescriptionNoName() {
        bootstrap.getTaskService().create(userId, "", "Desc");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testCreateWithDescriptionNoDescription() {
        bootstrap.getTaskService().create(userId, "AUTO Task", "");
    }

    @Test(expected = AccessDeniedException.class)
    public void testRemoveNoUserId() {
        bootstrap.getTaskService().remove("",null);
    }

    @Test
    public void testCreateFindAllRemove() {
        @Nullable Task task = new Task();
        task.setName("AUTO Task 3");
        task.setUserId(userId);
        bootstrap.getTaskService().add(userId, task);

        task = bootstrap.getTaskService().add(userId, null);
        Assert.assertNull(task);

        bootstrap.getTaskService().create(userId, "AUTO Task 2");
        bootstrap.getTaskService().create(userId, "AUTO Task 1", "desc");
        @Nullable List<Task> taskList = bootstrap.getTaskService().findAll(userId);
        Assert.assertEquals(3, taskList.size());
        Assert.assertEquals("AUTO Task 1", taskList.get(2).getName());

        taskList = bootstrap.getTaskService().findAll(userId, null);
        Assert.assertNull(taskList);

        taskList = bootstrap.getTaskService().findAll(userId, Sort.NAME.getComparator());
        Assert.assertEquals(3, taskList.size());
        Assert.assertEquals("AUTO Task 3", taskList.get(2).getName());

        @NotNull Optional<Task> taskOptional = bootstrap.getTaskService().remove(userId,null);
        Assert.assertFalse(taskOptional.isPresent());
        bootstrap.getTaskService().remove(userId, taskList.get(0));
        bootstrap.getTaskService().remove(userId, taskList.get(1));
        bootstrap.getTaskService().remove(userId, taskList.get(2));
        taskList = bootstrap.getTaskService().findAll(userId);
        Assert.assertEquals(0, taskList.size());
    }

    @Test(expected = AccessDeniedException.class)
    public void testFindByIdNoUserId() {
        bootstrap.getTaskService().findById("","1");
    }

    @Test(expected = EmptyIdException.class)
    public void testFindByIdNoId() {
        bootstrap.getTaskService().findById(userId,"");
    }

    @Test(expected = AccessDeniedException.class)
    public void testFindByIndexNoUserId() {
        bootstrap.getTaskService().findByIndex("",1);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIndexNoId() {
        bootstrap.getTaskService().findByIndex(userId,null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testFindByNameNoUserId() {
        bootstrap.getTaskService().findByName("","1");
    }

    @Test(expected = EmptyNameException.class)
    public void testFindByNameNoName() {
        bootstrap.getTaskService().findByName(userId,"");
    }

    @Test(expected = AccessDeniedException.class)
    public void testRemoveByIdNoUserId() {
        bootstrap.getTaskService().removeById("","1");
    }

    @Test(expected = EmptyIdException.class)
    public void testRemoveByIdNoId() {
        bootstrap.getTaskService().removeById(userId,"");
    }

    @Test(expected = AccessDeniedException.class)
    public void testRemoveByIndexNoUserId() {
        bootstrap.getTaskService().removeByIndex("",1);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIndexNoId() {
        bootstrap.getTaskService().removeByIndex(userId,null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testRemoveByNameNoUserId() {
        bootstrap.getTaskService().removeByName("","1");
    }

    @Test(expected = EmptyNameException.class)
    public void testRemoveByNameNoName() {
        bootstrap.getTaskService().removeByName(userId,"");
    }

    @Test
    public void testCreateFindRemove() {
        @NotNull final Task taskById = bootstrap.getTaskService().create(userId, "AUTO Task CFR Id");
        @NotNull final Task taskByIndex = bootstrap.getTaskService().create(userId, "AUTO Task CFR Index");
        @NotNull final Task taskByName = bootstrap.getTaskService().create(userId, "AUTO Task CFR Name");

        @NotNull Optional<Task> taskFound = bootstrap.getTaskService().findById(userId, taskById.getId());
        Assert.assertTrue(taskFound.isPresent());
        Assert.assertEquals("AUTO Task CFR Id", taskFound.get().getName());

        taskFound = bootstrap.getTaskService().findByIndex(userId, 1);
        Assert.assertTrue(taskFound.isPresent());
        Assert.assertEquals(taskByIndex.getId(), taskFound.get().getId());

        taskFound = bootstrap.getTaskService().findByName(userId, "AUTO Task CFR Name");
        Assert.assertTrue(taskFound.isPresent());
        Assert.assertEquals(taskByName.getId(), taskFound.get().getId());

        bootstrap.getTaskService().removeById(userId, taskById.getId());
        bootstrap.getTaskService().removeByIndex(userId, 0);
        bootstrap.getTaskService().removeByName(userId, "AUTO Task CFR Name");

        @Nullable final List<Task> taskList = bootstrap.getTaskService().findAll(userId);
        Assert.assertEquals(0, taskList.size());
    }

    @Test(expected = AccessDeniedException.class)
    public void testUpdateTaskByIdNoUserId() {
        bootstrap.getTaskService().updateTaskById("","1","123","456");
    }

    @Test(expected = EmptyIdException.class)
    public void testUpdateTaskByIdNoId() {
        bootstrap.getTaskService().updateTaskById(userId,"","123","456");
    }

    @Test(expected = EmptyNameException.class)
    public void testUpdateTaskByIdNoName() {
        bootstrap.getTaskService().updateTaskById(userId,"1","","456");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testUpdateTaskByIdNoDescription() {
        bootstrap.getTaskService().updateTaskById(userId,"1","123","");
    }

    @Test(expected = TaskNotFoundException.class)
    public void testUpdateTaskByIdNoTask() {
        bootstrap.getTaskService().updateTaskById(userId,"1","123","456");
    }

    @Test(expected = AccessDeniedException.class)
    public void testUpdateTaskByIndexNoUserId() {
        bootstrap.getTaskService().updateTaskByIndex("",1,"123","456");
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateTaskByIndexNoIndex() {
        bootstrap.getTaskService().updateTaskByIndex(userId, null,"123","456");
    }

    @Test(expected = EmptyNameException.class)
    public void testUpdateTaskByIndexNoName() {
        bootstrap.getTaskService().updateTaskByIndex(userId,1,"","456");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testUpdateTaskByIndexNoDescription() {
        bootstrap.getTaskService().updateTaskByIndex(userId,1,"123","");
    }

    @Test(expected = TaskNotFoundException.class)
    public void testUpdateTaskByIndexNoTask() {
        bootstrap.getTaskService().updateTaskByIndex(userId,1,"123","456");
    }

    @Test
    public void testUpdateTask() {
        @NotNull final Task taskById = bootstrap.getTaskService().create(userId,"AUTO", "AUTO");
        bootstrap.getTaskService().create(userId,"AUTO 2", "AUTO 2");

        @NotNull Task task = bootstrap.getTaskService().updateTaskById(userId,taskById.getId(),"AUTO1", "AUTO1D");
        Assert.assertEquals("AUTO1", task.getName());
        Assert.assertEquals("AUTO1D", task.getDescription());

        task = bootstrap.getTaskService().updateTaskByIndex(userId, 1, "AUTO2", "AUTO2D");
        Assert.assertEquals("AUTO2", task.getName());
        Assert.assertEquals("AUTO2D", task.getDescription());

        bootstrap.getTaskService().removeByName(userId, "AUTO1");
        bootstrap.getTaskService().removeByName(userId, "AUTO2");
    }
    
    @Test(expected = AccessDeniedException.class)
    public void testStartTaskByIdNoUserId() {
        bootstrap.getTaskService().startTaskById("","");
    }

    @Test(expected = EmptyIdException.class)
    public void testStartTaskByIdNoId() {
        bootstrap.getTaskService().startTaskById(userId,"");
    }

    @Test(expected = TaskNotFoundException.class)
    public void testStartTaskByIdNoTask() {
        bootstrap.getTaskService().startTaskById(userId,"1");
    }

    @Test(expected = AccessDeniedException.class)
    public void testStartTaskByIndexNoUserId() {
        bootstrap.getTaskService().startTaskByIndex("",1);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testStartTaskByIndexNoIndex() {
        bootstrap.getTaskService().startTaskByIndex(userId,null);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testStartTaskByIndexNoTask() {
        bootstrap.getTaskService().startTaskByIndex(userId,1);
    }

    @Test(expected = AccessDeniedException.class)
    public void testStartTaskByNameNoUserId() {
        bootstrap.getTaskService().startTaskByName("","123");
    }

    @Test(expected = EmptyNameException.class)
    public void testStartTaskByNameNoName() {
        bootstrap.getTaskService().startTaskByName(userId,null);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testStartTaskByNameNoTask() {
        bootstrap.getTaskService().startTaskByName(userId,"1");
    }

    @Test(expected = AccessDeniedException.class)
    public void testFinishTaskByIdNoUserId() {
        bootstrap.getTaskService().finishTaskById("","");
    }

    @Test(expected = EmptyIdException.class)
    public void testFinishTaskByIdNoId() {
        bootstrap.getTaskService().finishTaskById(userId,"");
    }

    @Test(expected = TaskNotFoundException.class)
    public void testFinishTaskByIdNoTask() {
        bootstrap.getTaskService().finishTaskById(userId,"1");
    }

    @Test(expected = AccessDeniedException.class)
    public void testFinishTaskByIndexNoUserId() {
        bootstrap.getTaskService().finishTaskByIndex("",1);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFinishTaskByIndexNoIndex() {
        bootstrap.getTaskService().finishTaskByIndex(userId,null);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testFinishTaskByIndexNoTask() {
        bootstrap.getTaskService().finishTaskByIndex(userId,1);
    }

    @Test(expected = AccessDeniedException.class)
    public void testFinishTaskByNameNoUserId() {
        bootstrap.getTaskService().finishTaskByName("","123");
    }

    @Test(expected = EmptyNameException.class)
    public void testFinishTaskByNameNoName() {
        bootstrap.getTaskService().finishTaskByName(userId,null);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testFinishTaskByNameNoTask() {
        bootstrap.getTaskService().finishTaskByName(userId,"1");
    }

    @Test(expected = AccessDeniedException.class)
    public void testChangeTaskStatusByIdNoUserId() {
        bootstrap.getTaskService().changeTaskStatusById("","", Status.IN_PROGRESS);
    }

    @Test(expected = EmptyIdException.class)
    public void testChangeTaskStatusByIdNoId() {
        bootstrap.getTaskService().changeTaskStatusById(userId,"", Status.IN_PROGRESS);
    }

    @Test(expected = EmptyStatusException.class)
    public void testChangeTaskStatusByIdNoStatus() {
        bootstrap.getTaskService().changeTaskStatusById(userId,"1", null);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testChangeTaskStatusByIdNoTask() {
        bootstrap.getTaskService().changeTaskStatusById(userId,"1", Status.IN_PROGRESS);
    }

    @Test(expected = AccessDeniedException.class)
    public void testChangeTaskStatusByIndexNoUserId() {
        bootstrap.getTaskService().changeTaskStatusByIndex("",1, Status.IN_PROGRESS);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeTaskStatusByIndexNoIndex() {
        bootstrap.getTaskService().changeTaskStatusByIndex(userId,null, Status.IN_PROGRESS);
    }

    @Test(expected = EmptyStatusException.class)
    public void testChangeTaskStatusByIndexNoStatus() {
        bootstrap.getTaskService().changeTaskStatusByIndex(userId,1, null);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testChangeTaskStatusByIndexNoTask() {
        bootstrap.getTaskService().changeTaskStatusByIndex(userId,1, Status.IN_PROGRESS);
    }

    @Test(expected = AccessDeniedException.class)
    public void testChangeTaskStatusByNameNoUserId() {
        bootstrap.getTaskService().changeTaskStatusByName("","123", Status.IN_PROGRESS);
    }

    @Test(expected = EmptyNameException.class)
    public void testChangeTaskStatusByNameNoName() {
        bootstrap.getTaskService().changeTaskStatusByName(userId,null, Status.IN_PROGRESS);
    }

    @Test(expected = EmptyStatusException.class)
    public void testChangeTaskStatusByNameNoStatus() {
        bootstrap.getTaskService().changeTaskStatusByName(userId,"1", null);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testChangeTaskStatusByNameNoTask() {
        bootstrap.getTaskService().changeTaskStatusByName(userId,"1", Status.IN_PROGRESS);
    }

    @Test
    public void testStatusField() {
        @NotNull Task taskById = bootstrap.getTaskService().create(userId, "AUTO Task Id");
        Assert.assertEquals(Status.NOT_STARTED, taskById.getStatus());
        @NotNull Task taskByIndex = bootstrap.getTaskService().create(userId, "AUTO Task Index");
        Assert.assertEquals(Status.NOT_STARTED, taskByIndex.getStatus());
        @NotNull Task taskByName = bootstrap.getTaskService().create(userId, "AUTO Task Name");
        Assert.assertEquals(Status.NOT_STARTED, taskByName.getStatus());

        taskById = bootstrap.getTaskService().startTaskById(userId, taskById.getId());
        Assert.assertEquals(Status.IN_PROGRESS, taskById.getStatus());
        taskByIndex = bootstrap.getTaskService().startTaskByIndex(userId, 1);
        Assert.assertEquals(Status.IN_PROGRESS, taskByIndex.getStatus());
        taskByName = bootstrap.getTaskService().startTaskByName(userId, "AUTO Task Name");
        Assert.assertEquals(Status.IN_PROGRESS, taskByName.getStatus());

        taskById = bootstrap.getTaskService().finishTaskById(userId, taskById.getId());
        Assert.assertEquals(Status.COMPLETED, taskById.getStatus());
        taskByIndex = bootstrap.getTaskService().finishTaskByIndex(userId, 1);
        Assert.assertEquals(Status.COMPLETED, taskByIndex.getStatus());
        taskByName = bootstrap.getTaskService().finishTaskByName(userId, "AUTO Task Name");
        Assert.assertEquals(Status.COMPLETED, taskByName.getStatus());

        taskById = bootstrap.getTaskService().changeTaskStatusById(userId, taskById.getId(), Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, taskById.getStatus());
        taskByIndex = bootstrap.getTaskService().changeTaskStatusByIndex(userId, 1, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, taskByIndex.getStatus());
        taskByName = bootstrap.getTaskService().changeTaskStatusByName(userId, "AUTO Task Name", Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, taskByName.getStatus());

        bootstrap.getTaskService().remove(taskById);
        bootstrap.getTaskService().remove(taskByIndex);
        bootstrap.getTaskService().remove(taskByName);
    }

    @Test(expected = AccessDeniedException.class)
    public void testFindAllByProjectIdNoUserId() {
        bootstrap.getProjectTaskService().findAllTaskByProjectId("","1");
    }

    @Test(expected = EmptyIdException.class)
    public void testFindAllByProjectIdNoProjectId() {
        bootstrap.getProjectTaskService().findAllTaskByProjectId(userId,"");
    }

    @Test(expected = AccessDeniedException.class)
    public void testBindTaskNoUserId() {
        bootstrap.getProjectTaskService().bindTaskByProjectId("", "1", "1");
    }

    @Test(expected = EmptyIdException.class)
    public void testBindTaskNoProjectId() {
        bootstrap.getProjectTaskService().bindTaskByProjectId(userId, "", "1");
    }

    @Test(expected = EmptyIdException.class)
    public void testBindTaskNoTaskId() {
        bootstrap.getProjectTaskService().bindTaskByProjectId(userId, "1", "");
    }

    @Test(expected = AccessDeniedException.class)
    public void testUnbindTaskNoUserId() {
        bootstrap.getProjectTaskService().unbindTaskByProjectId("", "1");
    }

    @Test(expected = EmptyIdException.class)
    public void testUnbindTaskNoTaskId() {
        bootstrap.getProjectTaskService().unbindTaskByProjectId(userId, "");
    }

    @Test
    public void testBindUnbindTask() {
        @NotNull final Project project = bootstrap.getProjectService().create(userId,"AUTO Project");
        @NotNull final String projectId = project.getId();
        @NotNull final Task taskFirst = bootstrap.getTaskService().create(userId, "AUTO Task 1");
        @NotNull final Task taskSecond = bootstrap.getTaskService().create(userId, "AUTO Task 2");

        bootstrap.getProjectTaskService().bindTaskByProjectId(userId, projectId, taskFirst.getId());
        bootstrap.getProjectTaskService().bindTaskByProjectId(userId, projectId, taskSecond.getId());
        @NotNull List<Task> taskList = bootstrap.getProjectTaskService().findAllTaskByProjectId(userId, projectId);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(projectId, taskList.get(0).getProjectId());
        Assert.assertEquals(projectId, taskList.get(1).getProjectId());

        bootstrap.getProjectTaskService().unbindTaskByProjectId(userId, taskFirst.getId());
        taskList = bootstrap.getProjectTaskService().findAllTaskByProjectId(userId, projectId);
        Assert.assertEquals(1, taskList.size());

        bootstrap.getTaskService().remove(taskFirst);
        bootstrap.getTaskService().remove(taskSecond);
        bootstrap.getProjectService().remove(project);
    }
    
}
