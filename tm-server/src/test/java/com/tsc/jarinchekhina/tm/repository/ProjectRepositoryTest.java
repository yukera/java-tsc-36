package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.entity.Project;
import com.tsc.jarinchekhina.tm.entity.User;
import com.tsc.jarinchekhina.tm.enumerated.Sort;
import org.jetbrains.annotations.NotNull;
import org.junit.*;

import java.util.List;

@FixMethodOrder
public class ProjectRepositoryTest {

    @NotNull
    private static final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private static final User user = bootstrap.getUserService().create("auto", "auto");

    @NotNull
    private static final String userId = user.getId();

    @AfterClass
    public static void after() {
        bootstrap.getUserRepository().removeByLogin("auto");
    }

    @Test
    public void testAddFindAll() {
        @NotNull final Project projectSecond = new Project();
        projectSecond.setName("AUTO 2");
        bootstrap.getProjectRepository().add(userId, projectSecond);
        @NotNull final Project projectFirst = new Project();
        projectFirst.setName("AUTO 1");
        bootstrap.getProjectRepository().add(userId, projectFirst);

        @NotNull List<Project> projectList = bootstrap.getProjectRepository().findAll(userId);
        Assert.assertEquals(2, projectList.size());
        projectList = bootstrap.getProjectRepository().findAll(userId, Sort.NAME.getComparator());
        Assert.assertEquals("AUTO 1", projectList.get(0).getName());

        bootstrap.getProjectRepository().remove(projectFirst);
        bootstrap.getProjectRepository().remove(projectSecond);
    }

    @Test
    public void testFindRemove() {
        @NotNull Project projectById = new Project();
        projectById.setName("AUTO Id");
        bootstrap.getProjectRepository().add(userId, projectById);
        @NotNull Project projectByIndex = new Project();
        projectByIndex.setName("AUTO Index");
        bootstrap.getProjectRepository().add(userId, projectByIndex);
        @NotNull Project projectByName = new Project();
        projectByName.setName("AUTO Name");
        bootstrap.getProjectRepository().add(userId, projectByName);

        projectById = bootstrap.getProjectRepository().findById(userId, projectById.getId()).get();
        Assert.assertEquals("AUTO Id", projectById.getName());
        projectByIndex = bootstrap.getProjectRepository().findByIndex(userId, 1).get();
        Assert.assertEquals("AUTO Index", projectByIndex.getName());
        projectByName = bootstrap.getProjectRepository().findByName(userId, "AUTO Name").get();
        Assert.assertEquals("AUTO Name", projectByName.getName());

        bootstrap.getProjectRepository().removeById(userId, projectById.getId());
        bootstrap.getProjectRepository().removeByIndex(userId, 0);
        bootstrap.getProjectRepository().removeByName(userId, "AUTO Name");

        @NotNull final List<Project> projectList = bootstrap.getProjectRepository().findAll(userId);
        Assert.assertEquals(0, projectList.size());
    }

}
