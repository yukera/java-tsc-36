package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.entity.Project;
import com.tsc.jarinchekhina.tm.entity.Task;
import com.tsc.jarinchekhina.tm.entity.User;
import com.tsc.jarinchekhina.tm.enumerated.Sort;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyDescriptionException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyNameException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyStatusException;
import com.tsc.jarinchekhina.tm.exception.entity.ProjectNotFoundException;
import com.tsc.jarinchekhina.tm.exception.system.IndexIncorrectException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

public class ProjectServiceTest {

    @NotNull
    private static final Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private static String userId;

    @BeforeClass
    public static void beforeClass() {
        @NotNull final User user = bootstrap.getUserService().create("auto", "auto");
        userId = user.getId();
    }

    @AfterClass
    public static void afterClass() {
        bootstrap.getProjectTaskService().clearProjects(userId);
        bootstrap.getUserRepository().removeByLogin("auto");
    }

    @Test(expected = AccessDeniedException.class)
    public void testFindAllNoUserId() {
        bootstrap.getProjectService().findAll("");
    }

    @Test(expected = AccessDeniedException.class)
    public void testFindAllWithComparatorNoUserId() {
        bootstrap.getProjectService().findAll("", Sort.NAME.getComparator());
    }

    @Test(expected = AccessDeniedException.class)
    public void testAddNoUserId() {
        bootstrap.getProjectService().add("", null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testCreateNoUserId() {
        bootstrap.getProjectService().create("", "AUTO Project");
    }

    @Test(expected = EmptyNameException.class)
    public void testCreateNoName() {
        bootstrap.getProjectService().create(userId, "");
    }

    @Test(expected = AccessDeniedException.class)
    public void testCreateWithDescriptionNoUserId() {
        bootstrap.getProjectService().create("", "AUTO Project", "Desc");
    }

    @Test(expected = EmptyNameException.class)
    public void testCreateWithDescriptionNoName() {
        bootstrap.getProjectService().create(userId, "", "Desc");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testCreateWithDescriptionNoDescription() {
        bootstrap.getProjectService().create(userId, "AUTO Project", "");
    }

    @Test(expected = AccessDeniedException.class)
    public void testRemoveNoUserId() {
        bootstrap.getProjectService().remove("",null);
    }

    @Test
    public void testCreateFindAllRemove() {
        @Nullable Project project = new Project();
        project.setName("AUTO Project 3");
        project.setUserId(userId);
        bootstrap.getProjectService().add(userId, project);

        project = bootstrap.getProjectService().add(userId, null);
        Assert.assertNull(project);

        bootstrap.getProjectService().create(userId, "AUTO Project 2");
        bootstrap.getProjectService().create(userId, "AUTO Project 1", "desc");
        @Nullable List<Project> projectList = bootstrap.getProjectService().findAll(userId);
        Assert.assertEquals(3, projectList.size());
        Assert.assertEquals("AUTO Project 3", projectList.get(2).getName());

        projectList = bootstrap.getProjectService().findAll(userId, null);
        Assert.assertNull(projectList);

        projectList = bootstrap.getProjectService().findAll(userId, Sort.CREATED.getComparator());
        Assert.assertEquals(3, projectList.size());
        Assert.assertEquals("AUTO Project 1", projectList.get(2).getName());

        @NotNull Optional<Project> projectOptional = bootstrap.getProjectService().remove(userId,null);
        Assert.assertFalse(projectOptional.isPresent());
        bootstrap.getProjectService().remove(userId, projectList.get(0));
        bootstrap.getProjectService().remove(userId, projectList.get(1));
        bootstrap.getProjectService().remove(userId, projectList.get(2));
        projectList = bootstrap.getProjectService().findAll(userId);
        Assert.assertEquals(0, projectList.size());
    }

    @Test(expected = AccessDeniedException.class)
    public void testFindByIdNoUserId() {
        bootstrap.getProjectService().findById("","1");
    }

    @Test(expected = EmptyIdException.class)
    public void testFindByIdNoId() {
        bootstrap.getProjectService().findById(userId,"");
    }

    @Test(expected = AccessDeniedException.class)
    public void testFindByIndexNoUserId() {
        bootstrap.getProjectService().findByIndex("",1);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIndexNoId() {
        bootstrap.getProjectService().findByIndex(userId,null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testFindByNameNoUserId() {
        bootstrap.getProjectService().findByName("","1");
    }

    @Test(expected = EmptyNameException.class)
    public void testFindByNameNoName() {
        bootstrap.getProjectService().findByName(userId,"");
    }

    @Test(expected = AccessDeniedException.class)
    public void testRemoveByIdNoUserId() {
        bootstrap.getProjectService().removeById("","1");
    }

    @Test(expected = EmptyIdException.class)
    public void testRemoveByIdNoId() {
        bootstrap.getProjectService().removeById(userId,"");
    }

    @Test(expected = AccessDeniedException.class)
    public void testRemoveByIndexNoUserId() {
        bootstrap.getProjectService().removeByIndex("",1);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIndexNoId() {
        bootstrap.getProjectService().removeByIndex(userId,null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testRemoveByNameNoUserId() {
        bootstrap.getProjectService().removeByName("","1");
    }

    @Test(expected = EmptyNameException.class)
    public void testRemoveByNameNoName() {
        bootstrap.getProjectService().removeByName(userId,"");
    }

    @Test
    public void testCreateFindRemove() {
        @NotNull final Project projectById = bootstrap.getProjectService().create(userId, "AUTO Project CFR Id");
        @NotNull final Project projectByIndex = bootstrap.getProjectService().create(userId, "AUTO Project CFR Index");
        @NotNull final Project projectByName = bootstrap.getProjectService().create(userId, "AUTO Project CFR Name");

        @NotNull Optional<Project> projectFound = bootstrap.getProjectService().findById(userId, projectById.getId());
        Assert.assertTrue(projectFound.isPresent());
        Assert.assertEquals("AUTO Project CFR Id", projectFound.get().getName());

        projectFound = bootstrap.getProjectService().findByIndex(userId, 1);
        Assert.assertTrue(projectFound.isPresent());
        Assert.assertEquals(projectByIndex.getId(), projectFound.get().getId());

        projectFound = bootstrap.getProjectService().findByName(userId, "AUTO Project CFR Name");
        Assert.assertTrue(projectFound.isPresent());
        Assert.assertEquals(projectByName.getId(), projectFound.get().getId());

        bootstrap.getProjectService().removeById(userId, projectById.getId());
        bootstrap.getProjectService().removeByIndex(userId, 0);
        bootstrap.getProjectService().removeByName(userId, "AUTO Project CFR Name");

        @Nullable final List<Project> projectList = bootstrap.getProjectService().findAll(userId);
        Assert.assertEquals(0, projectList.size());
    }

    @Test(expected = AccessDeniedException.class)
    public void testUpdateProjectByIdNoUserId() {
        bootstrap.getProjectService().updateProjectById("","1","123","456");
    }

    @Test(expected = EmptyIdException.class)
    public void testUpdateProjectByIdNoId() {
        bootstrap.getProjectService().updateProjectById(userId,"","123","456");
    }

    @Test(expected = EmptyNameException.class)
    public void testUpdateProjectByIdNoName() {
        bootstrap.getProjectService().updateProjectById(userId,"1","","456");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testUpdateProjectByIdNoDescription() {
        bootstrap.getProjectService().updateProjectById(userId,"1","123","");
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testUpdateProjectByIdNoProject() {
        bootstrap.getProjectService().updateProjectById(userId,"1","123","456");
    }

    @Test(expected = AccessDeniedException.class)
    public void testUpdateProjectByIndexNoUserId() {
        bootstrap.getProjectService().updateProjectByIndex("",1,"123","456");
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateProjectByIndexNoIndex() {
        bootstrap.getProjectService().updateProjectByIndex(userId, null,"123","456");
    }

    @Test(expected = EmptyNameException.class)
    public void testUpdateProjectByIndexNoName() {
        bootstrap.getProjectService().updateProjectByIndex(userId,1,"","456");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testUpdateProjectByIndexNoDescription() {
        bootstrap.getProjectService().updateProjectByIndex(userId,1,"123","");
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testUpdateProjectByIndexNoProject() {
        bootstrap.getProjectService().updateProjectByIndex(userId,1,"123","456");
    }

    @Test
    public void testUpdateProject() {
        @NotNull final Project projectById = bootstrap.getProjectService().create(userId,"AUTO", "AUTO");
        bootstrap.getProjectService().create(userId,"AUTO 2", "AUTO 2");

        @NotNull Project project = bootstrap.getProjectService().updateProjectById(userId,projectById.getId(),"AUTO1", "AUTO1D");
        Assert.assertEquals("AUTO1", project.getName());
        Assert.assertEquals("AUTO1D", project.getDescription());

        project = bootstrap.getProjectService().updateProjectByIndex(userId, 1, "AUTO2", "AUTO2D");
        Assert.assertEquals("AUTO2", project.getName());
        Assert.assertEquals("AUTO2D", project.getDescription());

        bootstrap.getProjectService().removeByName(userId, "AUTO1");
        bootstrap.getProjectService().removeByName(userId, "AUTO2");
    }
    
    @Test(expected = AccessDeniedException.class)
    public void testStartProjectByIdNoUserId() {
        bootstrap.getProjectService().startProjectById("","");
    }

    @Test(expected = EmptyIdException.class)
    public void testStartProjectByIdNoId() {
        bootstrap.getProjectService().startProjectById(userId,"");
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testStartProjectByIdNoProject() {
        bootstrap.getProjectService().startProjectById(userId,"1");
    }

    @Test(expected = AccessDeniedException.class)
    public void testStartProjectByIndexNoUserId() {
        bootstrap.getProjectService().startProjectByIndex("",1);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testStartProjectByIndexNoIndex() {
        bootstrap.getProjectService().startProjectByIndex(userId,null);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testStartProjectByIndexNoProject() {
        bootstrap.getProjectService().startProjectByIndex(userId,1);
    }

    @Test(expected = AccessDeniedException.class)
    public void testStartProjectByNameNoUserId() {
        bootstrap.getProjectService().startProjectByName("","123");
    }

    @Test(expected = EmptyNameException.class)
    public void testStartProjectByNameNoName() {
        bootstrap.getProjectService().startProjectByName(userId,null);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testStartProjectByNameNoProject() {
        bootstrap.getProjectService().startProjectByName(userId,"1");
    }

    @Test(expected = AccessDeniedException.class)
    public void testFinishProjectByIdNoUserId() {
        bootstrap.getProjectService().finishProjectById("","");
    }

    @Test(expected = EmptyIdException.class)
    public void testFinishProjectByIdNoId() {
        bootstrap.getProjectService().finishProjectById(userId,"");
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testFinishProjectByIdNoProject() {
        bootstrap.getProjectService().finishProjectById(userId,"1");
    }

    @Test(expected = AccessDeniedException.class)
    public void testFinishProjectByIndexNoUserId() {
        bootstrap.getProjectService().finishProjectByIndex("",1);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFinishProjectByIndexNoIndex() {
        bootstrap.getProjectService().finishProjectByIndex(userId,null);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testFinishProjectByIndexNoProject() {
        bootstrap.getProjectService().finishProjectByIndex(userId,1);
    }

    @Test(expected = AccessDeniedException.class)
    public void testFinishProjectByNameNoUserId() {
        bootstrap.getProjectService().finishProjectByName("","123");
    }

    @Test(expected = EmptyNameException.class)
    public void testFinishProjectByNameNoName() {
        bootstrap.getProjectService().finishProjectByName(userId,null);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testFinishProjectByNameNoProject() {
        bootstrap.getProjectService().finishProjectByName(userId,"1");
    }

    @Test(expected = AccessDeniedException.class)
    public void testChangeProjectStatusByIdNoUserId() {
        bootstrap.getProjectService().changeProjectStatusById("","", Status.IN_PROGRESS);
    }

    @Test(expected = EmptyIdException.class)
    public void testChangeProjectStatusByIdNoId() {
        bootstrap.getProjectService().changeProjectStatusById(userId,"", Status.IN_PROGRESS);
    }

    @Test(expected = EmptyStatusException.class)
    public void testChangeProjectStatusByIdNoStatus() {
        bootstrap.getProjectService().changeProjectStatusById(userId,"1", null);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testChangeProjectStatusByIdNoProject() {
        bootstrap.getProjectService().changeProjectStatusById(userId,"1", Status.IN_PROGRESS);
    }

    @Test(expected = AccessDeniedException.class)
    public void testChangeProjectStatusByIndexNoUserId() {
        bootstrap.getProjectService().changeProjectStatusByIndex("",1, Status.IN_PROGRESS);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeProjectStatusByIndexNoIndex() {
        bootstrap.getProjectService().changeProjectStatusByIndex(userId,null, Status.IN_PROGRESS);
    }

    @Test(expected = EmptyStatusException.class)
    public void testChangeProjectStatusByIndexNoStatus() {
        bootstrap.getProjectService().changeProjectStatusByIndex(userId,1, null);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testChangeProjectStatusByIndexNoProject() {
        bootstrap.getProjectService().changeProjectStatusByIndex(userId,1, Status.IN_PROGRESS);
    }

    @Test(expected = AccessDeniedException.class)
    public void testChangeProjectStatusByNameNoUserId() {
        bootstrap.getProjectService().changeProjectStatusByName("","123", Status.IN_PROGRESS);
    }

    @Test(expected = EmptyNameException.class)
    public void testChangeProjectStatusByNameNoName() {
        bootstrap.getProjectService().changeProjectStatusByName(userId,null, Status.IN_PROGRESS);
    }

    @Test(expected = EmptyStatusException.class)
    public void testChangeProjectStatusByNameNoStatus() {
        bootstrap.getProjectService().changeProjectStatusByName(userId,"1", null);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testChangeProjectStatusByNameNoProject() {
        bootstrap.getProjectService().changeProjectStatusByName(userId,"1", Status.IN_PROGRESS);
    }

    @Test
    public void testStatusField() {
        @NotNull Project projectById = bootstrap.getProjectService().create(userId, "AUTO Project Id");
        Assert.assertEquals(Status.NOT_STARTED, projectById.getStatus());
        @NotNull Project projectByIndex = bootstrap.getProjectService().create(userId, "AUTO Project Index");
        Assert.assertEquals(Status.NOT_STARTED, projectByIndex.getStatus());
        @NotNull Project projectByName = bootstrap.getProjectService().create(userId, "AUTO Project Name");
        Assert.assertEquals(Status.NOT_STARTED, projectByName.getStatus());

        projectById = bootstrap.getProjectService().startProjectById(userId, projectById.getId());
        Assert.assertEquals(Status.IN_PROGRESS, projectById.getStatus());
        projectByIndex = bootstrap.getProjectService().startProjectByIndex(userId, 1);
        Assert.assertEquals(Status.IN_PROGRESS, projectByIndex.getStatus());
        projectByName = bootstrap.getProjectService().startProjectByName(userId, "AUTO Project Name");
        Assert.assertEquals(Status.IN_PROGRESS, projectByName.getStatus());

        projectById = bootstrap.getProjectService().finishProjectById(userId, projectById.getId());
        Assert.assertEquals(Status.COMPLETED, projectById.getStatus());
        projectByIndex = bootstrap.getProjectService().finishProjectByIndex(userId, 1);
        Assert.assertEquals(Status.COMPLETED, projectByIndex.getStatus());
        projectByName = bootstrap.getProjectService().finishProjectByName(userId, "AUTO Project Name");
        Assert.assertEquals(Status.COMPLETED, projectByName.getStatus());

        projectById = bootstrap.getProjectService().changeProjectStatusById(userId, projectById.getId(), Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectById.getStatus());
        projectByIndex = bootstrap.getProjectService().changeProjectStatusByIndex(userId, 1, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectByIndex.getStatus());
        projectByName = bootstrap.getProjectService().changeProjectStatusByName(userId, "AUTO Project Name", Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectByName.getStatus());

        bootstrap.getProjectService().remove(projectById);
        bootstrap.getProjectService().remove(projectByIndex);
        bootstrap.getProjectService().remove(projectByName);
    }

    @Test(expected = AccessDeniedException.class)
    public void testRemoveProjectByIdNoUserId() {
        bootstrap.getProjectTaskService().removeProjectById("","1");
    }

    @Test(expected = EmptyIdException.class)
    public void testRemoveProjectByIdNoId() {
        bootstrap.getProjectTaskService().removeProjectById(userId,"");
    }

    @Test(expected = AccessDeniedException.class)
    public void testRemoveProjectByIdNoProject() {
        bootstrap.getProjectTaskService().removeProjectById(userId,"1");
    }

    @Test(expected = AccessDeniedException.class)
    public void testClearProjectsNoUserId() {
        bootstrap.getProjectTaskService().clearProjects("");
    }

    @Test
    public void testRemoveClearProjects() {
        @NotNull final Project projectFirst = bootstrap.getProjectService().create(userId, "AUTO Pr 1");
        @NotNull final Project projectSecond = bootstrap.getProjectService().create(userId, "AUTO Pr 2");
        bootstrap.getProjectService().create(userId, "AUTO Pr 3");
        @NotNull Task taskFirst = bootstrap.getTaskService().create(userId, "AUTO T 1");
        @NotNull Task taskSecond = bootstrap.getTaskService().create(userId, "AUTO T 2");
        @NotNull Task taskThird = bootstrap.getTaskService().create(userId, "AUTO T 3");

        bootstrap.getProjectTaskService().bindTaskByProjectId(userId, projectFirst.getId(), taskFirst.getId());
        bootstrap.getProjectTaskService().bindTaskByProjectId(userId, projectSecond.getId(), taskSecond.getId());
        bootstrap.getProjectTaskService().bindTaskByProjectId(userId, projectSecond.getId(), taskThird.getId());

        bootstrap.getProjectTaskService().removeProjectById(userId, projectFirst.getId());
        @NotNull Optional<Project> projectOptional = bootstrap.getProjectService().findByName(userId, "AUTO Pr 1");
        Assert.assertFalse(projectOptional.isPresent());
        projectOptional = bootstrap.getProjectService().findByName(userId, "AUTO Pr 2");
        Assert.assertTrue(projectOptional.isPresent());
        @NotNull Optional<Task> taskOptional = bootstrap.getTaskService().findByName(userId, "AUTO T 1");
        Assert.assertFalse(taskOptional.isPresent());
        taskOptional = bootstrap.getTaskService().findByName(userId, "AUTO T 2");
        Assert.assertTrue(taskOptional.isPresent());

        bootstrap.getProjectTaskService().clearProjects(userId);
        projectOptional = bootstrap.getProjectService().findByName(userId, "AUTO Pr 2");
        Assert.assertFalse(projectOptional.isPresent());
        projectOptional = bootstrap.getProjectService().findByName(userId, "AUTO Pr 3");
        Assert.assertFalse(projectOptional.isPresent());
        taskOptional = bootstrap.getTaskService().findByName(userId, "AUTO T 2");
        Assert.assertFalse(taskOptional.isPresent());
        taskOptional = bootstrap.getTaskService().findByName(userId, "AUTO T 3");
        Assert.assertFalse(taskOptional.isPresent());
    }
    
}
