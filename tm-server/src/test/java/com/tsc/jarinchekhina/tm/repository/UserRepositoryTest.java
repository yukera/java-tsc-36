package com.tsc.jarinchekhina.tm.repository;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.entity.User;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;

import java.util.Optional;

public class UserRepositoryTest {

    @NotNull
    private static final Bootstrap bootstrap = new Bootstrap();

    @Test
    public void testFindRemove() {
        @NotNull final User userFirst = new User();
        userFirst.setLogin("auto");
        userFirst.setEmail("auto email");
        bootstrap.getUserRepository().add(userFirst);
        @NotNull final User userSecond = new User();
        userSecond.setLogin("auto 2nd");
        userSecond.setEmail("auto 2nd email");
        bootstrap.getUserRepository().add(userSecond);

        @NotNull Optional<User> userOptionalFirst = bootstrap.getUserRepository().findByLogin("auto");
        Assert.assertEquals("auto", userOptionalFirst.get().getLogin());
        @NotNull Optional<User> userOptionalSecond = bootstrap.getUserRepository().findByEmail("auto 2nd email");
        Assert.assertEquals("auto 2nd", userOptionalSecond.get().getLogin());

        bootstrap.getUserRepository().removeByLogin("auto");
        bootstrap.getUserRepository().removeByLogin("auto 2nd");
        userOptionalFirst = bootstrap.getUserRepository().findByLogin("auto");
        Assert.assertFalse(userOptionalFirst.isPresent());
        userOptionalSecond = bootstrap.getUserRepository().findByEmail("auto 2nd email");
        Assert.assertFalse(userOptionalSecond.isPresent());
    }

}
