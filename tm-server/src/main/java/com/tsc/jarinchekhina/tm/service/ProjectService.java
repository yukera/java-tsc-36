package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.repository.IProjectRepository;
import com.tsc.jarinchekhina.tm.api.service.IProjectService;
import com.tsc.jarinchekhina.tm.entity.Project;
import com.tsc.jarinchekhina.tm.enumerated.Sort;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyDescriptionException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyNameException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyStatusException;
import com.tsc.jarinchekhina.tm.exception.entity.ProjectNotFoundException;
import com.tsc.jarinchekhina.tm.exception.system.IndexIncorrectException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectService(@NotNull final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        projectRepository.clear(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        @NotNull final Sort sortByName = Sort.NAME;
        @Nullable final List<Project> projectList = findAll(userId, sortByName.getComparator());
        if (projectList == null) throw new ProjectNotFoundException();
        return projectList;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId, @Nullable final Comparator<Project> comparator) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (comparator == null) return null;
        return projectRepository.findAll(userId, comparator);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project add(@Nullable final String userId, @Nullable final Project project) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (project == null) return null;
        return projectRepository.add(userId, project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project create(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(userId, project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(userId, project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Project> findById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        return projectRepository.findById(userId, id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Project> findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        return projectRepository.findByIndex(userId, index);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Project> findByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        return projectRepository.findByName(userId, name);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Project> remove(@Nullable final String userId, @Nullable final Project project) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (project == null) return Optional.empty();
        return projectRepository.remove(userId, project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Project> removeById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        return projectRepository.removeById(userId, id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Project> removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        return projectRepository.removeByIndex(userId, index);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Project> removeByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        return projectRepository.removeByName(userId, name);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateProjectById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
        @NotNull final Optional<Project> project = findById(userId, id);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> e.setName(name));
        project.ifPresent(e -> e.setDescription(description));
        return project.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateProjectByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
        @NotNull final Optional<Project> project = findByIndex(userId, index);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> e.setName(name));
        project.ifPresent(e -> e.setDescription(description));
        return project.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project startProjectById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        @NotNull final Optional<Project> project = findById(userId, id);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> e.setStatus(Status.IN_PROGRESS));
        return project.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project startProjectByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        @NotNull final Optional<Project> project = findByIndex(userId, index);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> e.setStatus(Status.IN_PROGRESS));
        return project.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project startProjectByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        @NotNull final Optional<Project> project = findByName(userId, name);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> e.setStatus(Status.IN_PROGRESS));
        return project.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project finishProjectById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        @NotNull final Optional<Project> project = findById(userId, id);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> e.setStatus(Status.COMPLETED));
        return project.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project finishProjectByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        @NotNull final Optional<Project> project = findByIndex(userId, index);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> e.setStatus(Status.COMPLETED));
        return project.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project finishProjectByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        @NotNull final Optional<Project> project = findByName(userId, name);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> e.setStatus(Status.COMPLETED));
        return project.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Optional<Project> project = findById(userId, id);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> e.setStatus(status));
        return project.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project changeProjectStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Optional<Project> project = findByIndex(userId, index);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> e.setStatus(status));
        return project.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project changeProjectStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Optional<Project> project = findByName(userId, name);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> e.setStatus(status));
        return project.get();
    }

}
