package com.tsc.jarinchekhina.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class SignatureUtil {

    @Nullable
    public static String sign(
            @Nullable final Object value,
            @NotNull final String secret,
            @NotNull final Integer iteration
    ) {
        try {
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            if (value == null) return null;
            @NotNull final String json = objectMapper.writeValueAsString(value);
            return sign(json, secret, iteration);
        } catch (final JsonProcessingException e) {
            return null;
        }
    }

    @Nullable
    public static String sign(
            @NotNull final String value,
            @NotNull final String secret,
            @NotNull final Integer iteration
    ) {
        @Nullable String result = value;
        for (int i = 0; i < iteration; i++) {
            result = HashUtil.md5(secret + result + secret);
        }
        return result;
    }

}
