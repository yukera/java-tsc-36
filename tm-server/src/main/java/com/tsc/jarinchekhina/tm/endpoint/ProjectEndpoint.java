package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.api.endpoint.IProjectEndpoint;
import com.tsc.jarinchekhina.tm.api.service.IServiceLocator;
import com.tsc.jarinchekhina.tm.entity.Project;
import com.tsc.jarinchekhina.tm.entity.Session;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.Optional;

@WebService
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public void clearProjects(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        serviceLocator.getProjectService().clear();
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> findAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getProjectService().findAll(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public Project createProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getProjectService().create(session.getUserId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public Project createProjectWithDescription(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getProjectService().create(session.getUserId(), name, description);
    }

    @Nullable
    @Override
    @WebMethod
    public Project findProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        @NotNull final Optional<Project> project = serviceLocator.getProjectService().findById(session.getUserId(), id);
        return project.orElse(null);
    }

    @Nullable
    @Override
    @WebMethod
    public Project findProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        @NotNull final Optional<Project> project = serviceLocator.getProjectService().findByIndex(session.getUserId(), index);
        return project.orElse(null);
    }

    @Nullable
    @Override
    @WebMethod
    public Project findProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        @NotNull final Optional<Project> project = serviceLocator.getProjectService().findByName(session.getUserId(), name);
        return project.orElse(null);
    }

    @Nullable
    @Override
    @WebMethod
    public Project removeProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        @NotNull Optional<Project> project = serviceLocator.getProjectTaskService().removeProjectById(session.getUserId(), id);
        return project.orElse(null);
    }

    @Nullable
    @Override
    @WebMethod
    public Project removeProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        @NotNull Optional<Project> project = serviceLocator.getProjectService().findByIndex(session.getUserId(), index);
        project = serviceLocator.getProjectTaskService().removeProjectById(session.getUserId(), project.get().getId());
        return project.orElse(null);
    }

    @Nullable
    @Override
    @WebMethod
    public Project removeProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        @NotNull Optional<Project> project = serviceLocator.getProjectService().findByName(session.getUserId(), name);
        project = serviceLocator.getProjectTaskService().removeProjectById(session.getUserId(), project.get().getId());
        return project.orElse(null);
    }

    @NotNull
    @Override
    @WebMethod
    public Project updateProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getProjectService().updateProjectById(session.getUserId(), id, name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public Project updateProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getProjectService().updateProjectByIndex(session.getUserId(), index, name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public Project startProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getProjectService().startProjectById(session.getUserId(), id);
    }

    @NotNull
    @Override
    @WebMethod
    public Project startProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getProjectService().startProjectByIndex(session.getUserId(), index);
    }

    @NotNull
    @Override
    @WebMethod
    public Project startProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getProjectService().startProjectByName(session.getUserId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public Project finishProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getProjectService().finishProjectById(session.getUserId(), id);
    }

    @NotNull
    @Override
    @WebMethod
    public Project finishProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getProjectService().finishProjectByIndex(session.getUserId(), index);
    }

    @NotNull
    @Override
    @WebMethod
    public Project finishProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getProjectService().finishProjectByName(session.getUserId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public Project changeProjectStatusById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getProjectService().changeProjectStatusById(session.getUserId(), id, status);
    }

    @NotNull
    @Override
    @WebMethod
    public Project changeProjectStatusByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getProjectService().changeProjectStatusByIndex(session.getUserId(), index, status);
    }

    @NotNull
    @Override
    @WebMethod
    public Project changeProjectStatusByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getProjectService().changeProjectStatusByName(session.getUserId(), name, status);
    }

}
