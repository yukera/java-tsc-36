package com.tsc.jarinchekhina.tm.api.other;

import org.jetbrains.annotations.NotNull;

public interface ISaltSetting {

    @NotNull
    String getPasswordSecret();

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getSessionSecret();

    @NotNull
    Integer getSessionIteration();

}
