package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.api.endpoint.IAdminUserEndpoint;
import com.tsc.jarinchekhina.tm.api.service.IServiceLocator;
import com.tsc.jarinchekhina.tm.entity.Session;
import com.tsc.jarinchekhina.tm.entity.User;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Optional;

@WebService
public class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    public AdminUserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @Override
    @WebMethod
    public User findByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.ADMIN);
        @NotNull final Optional<User> user = serviceLocator.getUserService().findByLogin(login);
        return user.orElse(null);
    }

    @Nullable
    @Override
    @WebMethod
    public User removeByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.ADMIN);
        @NotNull final Optional<User> user = serviceLocator.getUserService().removeByLogin(login);
        return user.orElse(null);
    }

    @Nullable
    @Override
    @WebMethod
    public User lockByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.ADMIN);
        @NotNull final Optional<User> user = serviceLocator.getUserService().lockByLogin(login);
        return user.orElse(null);
    }

    @Nullable
    @Override
    @WebMethod
    public User unlockByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.ADMIN);
        @NotNull final Optional<User> user = serviceLocator.getUserService().unlockByLogin(login);
        return user.orElse(null);
    }

}
