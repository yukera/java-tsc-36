package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.api.IService;
import com.tsc.jarinchekhina.tm.entity.User;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public interface IUserService extends IService<User> {

    boolean checkRoles(@Nullable String userId, @Nullable Role... roles);

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    Optional<User> findByLogin(@Nullable String login);

    @NotNull
    Optional<User> findByEmail(@Nullable String email);

    @NotNull
    Optional<User> removeByLogin(@Nullable String login);

    @NotNull
    Optional<User> lockByLogin(@Nullable String login);

    @NotNull
    Optional<User> unlockByLogin(@Nullable String login);

    @NotNull
    User setPassword(@Nullable String userId, @Nullable String password);

    @NotNull
    User updateUser(
            @Nullable String userId,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

}
