package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.repository.ITaskRepository;
import com.tsc.jarinchekhina.tm.api.service.ITaskService;
import com.tsc.jarinchekhina.tm.entity.Task;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyDescriptionException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyNameException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyStatusException;
import com.tsc.jarinchekhina.tm.exception.entity.TaskNotFoundException;
import com.tsc.jarinchekhina.tm.exception.system.IndexIncorrectException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    public TaskService(@NotNull final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        taskRepository.clear(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@Nullable final String userId) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        return taskRepository.findAll(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAll(@Nullable final String userId, @Nullable final Comparator<Task> comparator) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (comparator == null) return null;
        return taskRepository.findAll(userId, comparator);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task add(@Nullable final String userId, @Nullable final Task task) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (task == null) return null;
        return taskRepository.add(userId, task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task create(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        @NotNull final Task task = new Task();
        task.setName(name);
        taskRepository.add(userId, task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(userId, task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Task> findById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        return taskRepository.findById(userId, id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Task> findByIndex(@Nullable final String userId, final Integer index) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        return taskRepository.findByIndex(userId, index);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Task> findByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        return taskRepository.findByName(userId, name);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Task> remove(@Nullable final String userId, @Nullable final Task task) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (task == null) return Optional.empty();
        return taskRepository.remove(userId, task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Task> removeById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        return taskRepository.removeById(userId, id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Task> removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        return taskRepository.removeByIndex(userId, index);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Task> removeByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        return taskRepository.removeByName(userId, name);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateTaskById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
        @NotNull final Optional<Task> task = findById(userId, id);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(e -> e.setName(name));
        task.ifPresent(e -> e.setDescription(description));
        return task.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateTaskByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
        @NotNull final Optional<Task> task = findByIndex(userId, index);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(e -> e.setName(name));
        task.ifPresent(e -> e.setDescription(description));
        return task.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task startTaskById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        @NotNull final Optional<Task> task = findById(userId, id);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(e -> e.setStatus(Status.IN_PROGRESS));
        return task.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task startTaskByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        @NotNull final Optional<Task> task = findByIndex(userId, index);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(e -> e.setStatus(Status.IN_PROGRESS));
        return task.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task startTaskByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        @NotNull final Optional<Task> task = findByName(userId, name);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(e -> e.setStatus(Status.IN_PROGRESS));
        return task.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task finishTaskById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        @NotNull final Optional<Task> task = findById(userId, id);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(e -> e.setStatus(Status.COMPLETED));
        return task.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task finishTaskByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        @NotNull final Optional<Task> task = findByIndex(userId, index);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(e -> e.setStatus(Status.COMPLETED));
        return task.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task finishTaskByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        @NotNull final Optional<Task> task = findByName(userId, name);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(e -> e.setStatus(Status.COMPLETED));
        return task.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Optional<Task> task = findById(userId, id);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(e -> e.setStatus(status));
        return task.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Optional<Task> task = findByIndex(userId, index);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(e -> e.setStatus(status));
        return task.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task changeTaskStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Optional<Task> task = findByName(userId, name);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(e -> e.setStatus(status));
        return task.get();
    }

}
