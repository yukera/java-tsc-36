package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.api.endpoint.IAdminEndpoint;
import com.tsc.jarinchekhina.tm.api.service.IServiceLocator;
import com.tsc.jarinchekhina.tm.entity.Session;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    public AdminEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public void saveBackup(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.ADMIN);
        serviceLocator.getDataService().saveBackup();
    }

    @Override
    @WebMethod
    public void loadBackup(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.ADMIN);
        serviceLocator.getDataService().loadBackup();
    }

    @Override
    @WebMethod
    public void saveBinaryData(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.ADMIN);
        serviceLocator.getDataService().saveBinaryData();
    }

    @Override
    @WebMethod
    public void loadBinaryData(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.ADMIN);
        serviceLocator.getDataService().loadBinaryData();
    }

    @Override
    @WebMethod
    public void saveBase64Data(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.ADMIN);
        serviceLocator.getDataService().saveBase64Data();
    }

    @Override
    @WebMethod
    public void loadBase64Data(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.ADMIN);
        serviceLocator.getDataService().loadBase64Data();
    }

    @Override
    @WebMethod
    public void saveFasterXmlData(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "isJson", partName = "isJson") final boolean isJson
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.ADMIN);
        serviceLocator.getDataService().saveFasterXmlData(isJson);
    }

    @Override
    @WebMethod
    public void loadFasterXmlData(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "isJson", partName = "isJson") final boolean isJson
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.ADMIN);
        serviceLocator.getDataService().loadFasterXmlData(isJson);
    }

    @Override
    @WebMethod
    public void saveJaxbData(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "isJson", partName = "isJson") final boolean isJson
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.ADMIN);
        serviceLocator.getDataService().saveJaxbData(isJson);
    }

    @Override
    @WebMethod
    public void loadJaxbData(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "isJson", partName = "isJson") final boolean isJson
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.ADMIN);
        serviceLocator.getDataService().loadJaxbData(isJson);
    }

}
