package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.repository.IUserRepository;
import com.tsc.jarinchekhina.tm.api.service.IPropertyService;
import com.tsc.jarinchekhina.tm.api.service.IUserService;
import com.tsc.jarinchekhina.tm.entity.User;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.exception.empty.*;
import com.tsc.jarinchekhina.tm.exception.system.HashIncorrectException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.exception.user.EmailRegisteredException;
import com.tsc.jarinchekhina.tm.exception.user.LoginExistsException;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import com.tsc.jarinchekhina.tm.util.HashUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull final IUserRepository userRepository,
            @NotNull final IPropertyService propertyService) {
        super(userRepository);
        this.userRepository = userRepository;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        if (DataUtil.isEmpty(password)) throw new EmptyPasswordException();
        if (findByLogin(login).isPresent()) throw new LoginExistsException(login);
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        @Nullable final String hashPassword = HashUtil.salt(propertyService, password);
        if (DataUtil.isEmpty(hashPassword)) throw new HashIncorrectException();
        user.setPasswordHash(hashPassword);
        return userRepository.add(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        if (DataUtil.isEmpty(password)) throw new EmptyPasswordException();
        if (DataUtil.isEmpty(email)) throw new EmptyEmailException();
        if (findByLogin(login).isPresent()) throw new LoginExistsException(login);
        if (findByEmail(email).isPresent()) throw new EmailRegisteredException(email);
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        if (DataUtil.isEmpty(password)) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        if (findByLogin(login).isPresent()) throw new LoginExistsException(login);
        @NotNull final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<User> findByLogin(@Nullable final String login) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<User> findByEmail(@Nullable final String email) {
        if (DataUtil.isEmpty(email)) throw new EmptyEmailException();
        return userRepository.findByEmail(email);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<User> removeByLogin(@Nullable final String login) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        return userRepository.removeByLogin(login);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<User> lockByLogin(@Nullable final String login) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        @NotNull final Optional<User> user = findByLogin(login);
        if (!user.isPresent()) throw new AccessDeniedException();
        user.get().setLocked(true);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<User> unlockByLogin(@Nullable final String login) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        @NotNull final Optional<User> user = findByLogin(login);
        if (!user.isPresent()) throw new AccessDeniedException();
        user.get().setLocked(false);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User setPassword(@Nullable final String userId, @Nullable final String password) {
        if (DataUtil.isEmpty(userId)) throw new EmptyIdException();
        if (DataUtil.isEmpty(password)) throw new EmptyPasswordException();
        @NotNull final Optional<User> user = findById(userId);
        if (!user.isPresent()) throw new AccessDeniedException();
        @Nullable final String hashPassword = HashUtil.salt(propertyService, password);
        if (DataUtil.isEmpty(hashPassword)) throw new HashIncorrectException();
        user.ifPresent(e -> e.setPasswordHash(hashPassword));
        return user.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (DataUtil.isEmpty(userId)) throw new EmptyIdException();
        @NotNull final Optional<User> user = findById(userId);
        if (!user.isPresent()) throw new AccessDeniedException();
        user.ifPresent(e -> e.setFirstName(firstName));
        user.ifPresent(e -> e.setLastName(lastName));
        user.ifPresent(e -> e.setMiddleName(middleName));
        return user.get();
    }

    @Override
    @SneakyThrows
    public boolean checkRoles(@Nullable final String userId, @Nullable final Role... roles) {
        if (DataUtil.isEmpty(userId)) return false;
        if (roles == null) return true;
        @Nullable final Optional<User> user = findById(userId);
        if (!user.isPresent()) throw new AccessDeniedException();
        @NotNull final Role role = user.get().getRole();
        for (@NotNull final Role item : roles) {
            if (item.equals(role)) return true;
        }
        return false;
    }

}
