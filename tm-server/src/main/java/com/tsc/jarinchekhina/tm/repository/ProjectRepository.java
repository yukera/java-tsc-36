package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.repository.IProjectRepository;
import com.tsc.jarinchekhina.tm.entity.Project;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@NoArgsConstructor
public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @NotNull
    private Predicate<Project> predicateByUser(@NotNull final String userId) {
        return e -> userId.equals(e.getUserId());
    }

    @NotNull
    private Predicate<Project> predicateByName(@NotNull final String name) {
        return e -> name.equals(e.getName());
    }

    @Override
    public void clear(@NotNull final String userId) {
        List<Project> entitiesNew = new ArrayList<>();
        for (final Project project : entities) {
            if (!userId.equals(project.getUserId())) entitiesNew.add(project);
        }
        entities = entitiesNew;
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        return entities.stream()
                .filter(predicateByUser(userId))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId, @NotNull final Comparator<Project> comparator) {
        return entities.stream()
                .filter(predicateByUser(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Project add(@NotNull final String userId, @NotNull final Project project) {
        project.setUserId(userId);
        entities.add(project);
        return project;
    }

    @NotNull
    @Override
    public Optional<Project> findById(@NotNull final String userId, @NotNull final String id) {
        return entities.stream()
                .filter(predicateByUser(userId))
                .filter(predicateById(id))
                .findFirst();
    }

    @NotNull
    @Override
    public Optional<Project> findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return entities.stream()
                .filter(predicateByUser(userId))
                .skip(index)
                .findFirst();
    }

    @NotNull
    @Override
    public Optional<Project> findByName(@NotNull final String userId, @NotNull final String name) {
        return entities.stream()
                .filter(predicateByUser(userId))
                .filter(predicateByName(name))
                .findFirst();
    }

    @NotNull
    @Override
    public Optional<Project> remove(@NotNull final String userId, @NotNull final Project project) {
        if (!userId.equals(project.getUserId())) return Optional.empty();
        entities.remove(project);
        return Optional.of(project);
    }

    @NotNull
    @Override
    public Optional<Project> removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Optional<Project> project = findById(userId, id);
        project.ifPresent(e -> entities.remove(e));
        return project;
    }

    @NotNull
    @Override
    public Optional<Project> removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Optional<Project> project = findByIndex(userId, index);
        project.ifPresent(e -> entities.remove(e));
        return project;
    }

    @NotNull
    @Override
    public Optional<Project> removeByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Optional<Project> project = findByName(userId, name);
        project.ifPresent(e -> entities.remove(e));
        return project;
    }

}
