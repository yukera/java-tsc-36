package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.repository.ITaskRepository;
import com.tsc.jarinchekhina.tm.entity.Task;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@NoArgsConstructor
public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @NotNull
    private Predicate<Task> predicateByUser(@NotNull final String userId) {
        return e -> userId.equals(e.getUserId());
    }

    @NotNull
    private Predicate<Task> predicateByProject(@NotNull final String projectId) {
        return e -> projectId.equals(e.getProjectId());
    }

    @NotNull
    private Predicate<Task> predicateByName(@NotNull final String name) {
        return e -> name.equals(e.getName());
    }

    @Override
    public void clear(@NotNull final String userId) {
        List<Task> entitiesNew = new ArrayList<>();
        for (final Task task : entities) {
            if (!userId.equals(task.getUserId())) entitiesNew.add(task);
        }
        entities = entitiesNew;

    }

    @Override
    public void removeAllByProjectId(@NotNull final String projectId) {
        List<Task> entitiesNew = new ArrayList<>();
        for (final Task task : entities) {
            if (!projectId.equals(task.getProjectId())) entitiesNew.add(task);
        }
        entities = entitiesNew;

    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId) {
        return entities.stream()
                .filter(predicateByUser(userId))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId, @NotNull final Comparator<Task> comparator) {
        return entities.stream()
                .filter(predicateByUser(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return entities.stream()
                .filter(predicateByUser(userId))
                .filter(predicateByProject(projectId))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Task add(@NotNull final String userId, @NotNull final Task task) {
        task.setUserId(userId);
        entities.add(task);
        return task;
    }

    @NotNull
    @Override
    public Optional<Task> findById(@NotNull final String userId, @NotNull final String id) {
        return entities.stream()
                .filter(predicateByUser(userId))
                .filter(predicateById(id))
                .findFirst();
    }

    @NotNull
    @Override
    public Optional<Task> findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return entities.stream()
                .filter(predicateByUser(userId))
                .skip(index)
                .findFirst();
    }

    @NotNull
    @Override
    public Optional<Task> findByName(@NotNull final String userId, @NotNull final String name) {
        return entities.stream()
                .filter(predicateByUser(userId))
                .filter(predicateByName(name))
                .findFirst();
    }

    @NotNull
    @Override
    public Optional<Task> remove(@NotNull final String userId, @NotNull final Task task) {
        if (!userId.equals(task.getUserId())) return Optional.empty();
        entities.remove(task);
        return Optional.of(task);
    }

    @NotNull
    @Override
    public Optional<Task> removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Optional<Task> task = findById(userId, id);
        task.ifPresent(e -> entities.remove(e));
        return task;
    }

    @NotNull
    @Override
    public Optional<Task> removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Optional<Task> task = findByIndex(userId, index);
        task.ifPresent(e -> entities.remove(e));
        return task;
    }

    @NotNull
    @Override
    public Optional<Task> removeByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Optional<Task> task = findByName(userId, name);
        task.ifPresent(e -> entities.remove(e));
        return task;
    }

}
